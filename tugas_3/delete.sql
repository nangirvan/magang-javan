-- Menggunakan EXTRACT
DELETE FROM karyawan WHERE EXTRACT(MONTH FROM tanggal_masuk) = 9;

-- Menggunakan DATE_PART
DELETE FROM karyawan WHERE DATE_PART('month', tanggal_masuk) = 9;