CREATE DATABASE sdm;

CREATE TABLE departemen(
    id SERIAL PRIMARY KEY,
    nama TEXT NOT NULL UNIQUE
);

CREATE TABLE karyawan(
    id SERIAL PRIMARY KEY,
    nama TEXT NOT NULL UNIQUE,
    jenis_kelamin CHAR(1) NOT NULL,
    status VARCHAR(10) NOT NULL,
    tanggal_lahir DATE NOT NULL,
    tanggal_masuk DATE NOT NULL,
    departemen INT NOT NULL REFERENCES departemen(id) ON DELETE SET NULL ON UPDATE CASCADE
);
