-- Membuat tabel baru yaitu role dari karyawan
CREATE TABLE role (
    id SERIAL PRIMARY KEY,
    title TEXT UNIQUE NOT NULL
);

-- Insert data ke role
INSERT INTO role(id, title)
VALUES
       (1, 'Direktur'),
       (2, 'Manajer'),
       (3, 'Staff');

-- Menambahkan kolom role ke tabel karyawan
ALTER TABLE karyawan ADD COLUMN role INT REFERENCES role(id) ON DELETE RESTRICT ON UPDATE CASCADE;

-- Update data role setiap karyawan
UPDATE karyawan SET role=1 WHERE departemen=1 AND nama='Rizki Saputra';
UPDATE karyawan SET role=2 WHERE departemen=1 AND nama IN ('Farhan Reza', 'Riyando Adi');
UPDATE karyawan SET role=3 WHERE departemen NOT IN (1) AND nama NOT IN ('Rizki Saputra', 'Farhan Reza', 'Riyando Adi');

-- Menambahkan rule NOT NULL ke kolom role di tabel karyawan
ALTER TABLE karyawan ALTER COLUMN role SET NOT NULL;

-- Select semua karyawan dengan role di bawah manager
SELECT id, nama FROM karyawan WHERE role > 2 ORDER BY id;
