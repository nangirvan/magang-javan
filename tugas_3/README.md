## Tugas 3

Database yang digunakan ada PostgreSQL 12


## Struktur Organisasi
- Terdapat tabel baru yaitu "role" yang berisi jabatan dari karyawan
- Terdapat penambahan kolom "role" pada tabel karyawan yang mengarah ke tabel role
- Role paling tinggi adalah role dengan id paling kecil
- Sehingga, semua karyawan di bawah manager merupakan karyawan yang mempunyai id role lebih besar dari manager

