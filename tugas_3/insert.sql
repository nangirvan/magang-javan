INSERT INTO departemen(id, nama)
VALUES
       (1, 'Manajemen'),
       (2, 'Pengembangan Bisnis'),
       (3, 'Teknisi'),
       (4, 'Analis');


INSERT INTO karyawan(id, nama, jenis_kelamin, status, tanggal_lahir, tanggal_masuk, departemen)
VALUES
       (1, 'Rizki Saputra', 'L', 'Menikah', '11/10/1980', '1/1/2011', 1),
       (2, 'Farhan Reza', 'L', 'Belum', '1/11/1989', '1/1/2011', 1),
       (3, 'Riyando Adi', 'L', 'Menikah', '25/1/1977', '1/1/2011', 1),
       (4, 'Diego Manuel', 'L', 'Menikah', '22/2/1983', '4/9/2012', 2),
       (5, 'Satya Laksana', 'L', 'Menikah', '12/1/1981', '19/3/2011', 2),
       (6, 'Miguel Hernandez', 'L', 'Menikah', '16/10/1994', '15/6/2014', 2),
       (7, 'Putri Persada', 'P', 'Menikah', '30/1/1988', '14/4/2013', 2),
       (8, 'Alma Safira', 'P', 'Menikah', '18/5/1991', '28/9/2013', 3),
       (9, 'Haqi Hafiz', 'L', 'Belum', '19/9/1995', '9/3/2015', 3),
       (10, 'Abi Isyawara', 'L', 'Belum', '3/6/1991', '22/1/2012', 3),
       (11, 'Maman Kresna', 'L', 'Belum', '21/8/1993', '15/9/2012', 3),
       (12, 'Nadia Aulia', 'P', 'Belum', '7/10/1989', '7/5/2012', 4),
       (13, 'Mutiara Rezki', 'P', 'Menikah', '23/3/1988', '21/5/2013', 4),
       (14, 'Dani Setiawan', 'L', 'Belum', '11/2/1986', '30/11/2014', 4),
       (15, 'Budi Putra', 'L', 'Belum', '23/10/1995', '3/12/2015', 4);