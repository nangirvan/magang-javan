<?php

// Fungsi untuk mendapatkan huruf vokal dari string
function getVocalLetterFromString($str)
{
    $vocal_letter = ['a', 'i', 'u', 'e', 'o'];
    $result = [];

    foreach ($vocal_letter as $letter) {
        if (strpos($str, $letter)) {
            $result[] = $letter;
        }
    }

    // nilai kembalian berupa array berisi huruf vokal yang terdapat pada string
    // panjang array sejumlah huruf vokal yang ditemukan
    return $result;
}

// Fungsi untuk melakukan tes output dari fungsi getVocalLetterFromString
function tesOutput($str)
{
    $result_vocal_letter = getVocalLetterFromString($str);
    $count_vocal_letter = count($result_vocal_letter);

    echo "$str = $count_vocal_letter yaitu ";
    foreach ($result_vocal_letter as $letter) {
        echo "$letter ";
    }
}

tesOutput('wisnu');
echo '<br>';
tesOutput('bayu');
echo '<br>';
tesOutput('manupraba');