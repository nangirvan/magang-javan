<?php

function calculate($str)
{
    $valid_operators = ['+', '-', '*', '/'];
    $arr_str = explode(' ', $str);

    for ($i=0; $i<count($arr_str); $i++) {
        // Mengecek apakah inputan merupakan bilangan
        if ($i%2 == 0) {
            if (!is_numeric($arr_str[$i]))
                return 'Input harus bilangan';
        }
        // Mengecek apakah operator valid
        else {
            if (strtolower($arr_str[$i]) === 'x'){
                $str = str_replace('x', '*', $str);
            } else if (!in_array($arr_str[$i], $valid_operators)) {
                return 'Operator tidak valid';
            }
        }
    }

    // Melakukan perhitungan
    $result = eval("return $str;");
    return (is_infinite($result)) ? 'Tidak bisa dilakukan' : $result;
}


// Tes Output
echo calculate("2 + 2");
echo "<br>";
echo calculate("2 x 2");
echo "<br>";
echo calculate("2 / 2");
echo "<br>";
echo calculate("2 / 0");