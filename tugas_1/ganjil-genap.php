<?php

// Fungsi untuk mengecek apakah sebuah bilangan genap atau ganjil
function isNumberEvenOrOdd($number)
{
    if (is_numeric($number)) {
        return ($number % 2 == 0) ? 'genap' : 'ganjil';
    }
    return false;
}

// Fungsi untuk mencetak angka dari parameter 1 ke parameter 2 beserta statusnya genap atau ganjil
function printNumbersWithStatus($a, $b)
{
    if (($a != null) && ($b != null)) {
        for ($i=$a; $i<=$b; $i++) {
            echo "Angka $i adalah ".isNumberEvenOrOdd($i).'<br>';
        }
    } else {
        echo 'parameter cannot null';
    }
}

printNumbersWithStatus(1, 4);
echo '<br>';
printNumbersWithStatus(10, 11);
echo '<br>';
printNumbersWithStatus(6, 8);