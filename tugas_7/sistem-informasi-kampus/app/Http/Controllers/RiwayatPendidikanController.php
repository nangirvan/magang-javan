<?php

namespace App\Http\Controllers;

use App\Http\Requests\RiwayatPendidikanEditValidation;
use Illuminate\Http\Request;
use App\Models\Dosen;
use App\Models\RiwayatPendidikan;
use App\Http\Requests\RiwayatPendidikanAddValidation;

class RiwayatPendidikanController extends Controller
{
    public function index(Request $request)
    {
        $data = [
            'selected_dosen' => Dosen::find($request->dosen_id),
            'all_riwayat_pendidikan' => RiwayatPendidikan::where('dosen_id', $request->dosen_id)->orderBy('id')->get(),
        ];

        return view('riwayat-pendidikan', $data);
    }

    public function showFormAdd(Request $request)
    {
        $data = [
            'selected_dosen' => Dosen::find($request->dosen_id),
        ];

        return view('riwayat-pendidikan-form-add', $data);
    }

    public function showFormEdit(Request $request)
    {
        $data = [
            'selected_riwayat_pendidikan' => RiwayatPendidikan::find($request->id),
        ];

        return view('riwayat-pendidikan-form-edit', $data);
    }

    public function add(RiwayatPendidikanAddValidation $request)
    {
        $data = $request->validated();

        RiwayatPendidikan::create($data);

        return redirect('/riwayat-pendidikan/'.$data['dosen_id'])->with('success', 'Riwayat Pendidikan berhasil ditambahkan!');
    }

    public function edit(RiwayatPendidikanEditValidation $request)
    {
        $data = $request->validated();

        RiwayatPendidikan::where('id', $data['id'])->update($data);

        return redirect('/riwayat-pendidikan/'.$data['dosen_id'])->with('success', 'Riwayat Pendidikan berhasil dirubah!');
    }

    public function delete(Request $request)
    {
        RiwayatPendidikan::find($request->id)->delete();

        return redirect('/riwayat-pendidikan/'.$request->dosen_id)->with('success', 'Riwayat Pendidikan berhasil dihapus!');
    }
}
