<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dosen;
use App\Models\MataKuliah;
use App\Http\Requests\DosenMataKuliahAddValidation;

class DosenMataKuliahController extends Controller
{
    public function index(Request $request)
    {
        $dosen = Dosen::find($request->dosen_id);

        $data = [
            'selected_dosen' => $dosen,
            'all_mata_kuliah' => $dosen->mata_kuliah()->get(),
        ];

        return view('dosen-mata-kuliah', $data);
    }

    public function showFormAdd(Request $request)
    {
        $data = [
            'selected_dosen' => Dosen::find($request->dosen_id),
            'all_mata_kuliah' => MataKuliah::all(),
        ];

        return view('dosen-mata-kuliah-form-add', $data);
    }

    public function add(DosenMataKuliahAddValidation $request)
    {
        $data = $request->validated();

        Dosen::find($data['dosen_id'])->mata_kuliah()->sync($data['mata_kuliah_id'], false);

        return redirect('/dosen/'.$data['dosen_id'].'/mata-kuliah')->with('success', 'Mata Kuliah berhasil ditambahkan!');
    }

    public function delete(Request $request)
    {
        Dosen::find($request->dosen_id)->mata_kuliah()->detach($request->mata_kuliah_id);

        return redirect('/dosen/'.$request->dosen_id.'/mata-kuliah')->with('success', 'Mata Kuliah berhasil ditambahkan!');
    }
}
