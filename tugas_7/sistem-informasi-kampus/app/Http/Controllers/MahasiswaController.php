<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use App\Http\Requests\MahasiswaAddValidation;
use App\Http\Requests\MahasiswaEditValidation;

class MahasiswaController extends Controller
{
    public function index()
    {
        $data = [
            'all_mahasiswa' => Mahasiswa::all()->sortBy('id'),
        ];

        return view('mahasiswa', $data);
    }

    public function showFormAdd()
    {
        return view('mahasiswa-form-add');
    }

    public function showFormEdit(Request $request)
    {
        $data = [
            'selected_mahasiswa' => Mahasiswa::find($request->id),
        ];

        return view('mahasiswa-form-edit', $data);
    }

    public function add(MahasiswaAddValidation $request)
    {
        $data = $request->validated();

        Mahasiswa::create($data);

        return redirect('/mahasiswa')->with('success', 'Mahasiswa berhasil ditambahkan!');
    }

    public function edit(MahasiswaEditValidation $request)
    {
        $data = $request->validated();

        Mahasiswa::where('id', $data['id'])->update($data);

        return redirect('/mahasiswa')->with('success', 'Mahasiswa berhasil dirubah!');
    }

    public function delete(Request $request)
    {
        Mahasiswa::find($request->id)->delete();

        return redirect('/mahasiswa')->with('success', 'Mahasiswa berhasil dihapus!');
    }
}
