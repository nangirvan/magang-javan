<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MataKuliah;
use App\Http\Requests\MataKuliahAddValidation;
use App\Http\Requests\MataKuliahEditValidation;

class MataKuliahController extends Controller
{
    public function index()
    {
        $data = [
            'all_mata_kuliah' => MataKuliah::all()->sortBy('id'),
        ];

        return view('mata-kuliah', $data);
    }

    public function showFormAdd()
    {
        return view('mata-kuliah-form-add');
    }

    public function showFormEdit(Request $request)
    {
        $data = [
            'selected_mata_kuliah' => MataKuliah::find($request->id),
        ];

        return view('mata-kuliah-form-edit', $data);
    }

    public function add(MataKuliahAddValidation $request)
    {
        $data = $request->validated();

        MataKuliah::create($data);

        return redirect('/mata-kuliah')->with('success', 'Mata Kuliah berhasil ditambahkan!');
    }

    public function edit(MataKuliahEditValidation $request)
    {
        $data = $request->validated();

        MataKuliah::where('id', $data['id'])->update($data);

        return redirect('/mata-kuliah')->with('success', 'Mata Kuliah berhasil dirubah!');
    }

    public function delete(Request $request)
    {
        MataKuliah::find($request->id)->delete();

        return redirect('/mata-kuliah')->with('success', 'Mata Kuliah berhasil dihapus!');
    }
}
