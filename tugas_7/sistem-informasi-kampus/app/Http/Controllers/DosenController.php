<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dosen;
use App\Http\Requests\DosenAddValidation;
use App\Http\Requests\DosenEditValidation;

class DosenController extends Controller
{
    public function index()
    {
        $data = [
            'all_dosen' => Dosen::with('riwayat_pendidikan', 'mata_kuliah')->orderBy('id')->get(),
        ];
//        return $data;
        return view('dosen', $data);
    }

    public function showFormAdd()
    {
        return view('dosen-form-add');
    }

    public function showFormEdit(Request $request)
    {
        $data = [
            'selected_dosen' => Dosen::find($request->id),
        ];

        return view('dosen-form-edit', $data);
    }

    public function add(DosenAddValidation $request)
    {
        $data = $request->validated();

        Dosen::create($data);

        return redirect('/dosen')->with('success', 'Dosen berhasil ditambahkan!');
    }

    public function edit(DosenEditValidation $request)
    {
        $data = $request->validated();

        Dosen::where('id', $data['id'])->update($data);

        return redirect('/dosen')->with('success', 'Dosen berhasil dirubah!');
    }

    public function delete(Request $request)
    {
        Dosen::find($request->id)->delete();

        return redirect('/dosen')->with('success', 'Dosen berhasil dihapus!');
    }
}
