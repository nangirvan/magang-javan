<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use App\Models\MataKuliah;
use App\Http\Requests\KelasMahasiswaAddvalidation;

class KelasController extends Controller
{
    public function index()
    {
        $mahasiswa = Mahasiswa::all();

        foreach ($mahasiswa as $item) {
            $item->total_sks = $item->mata_kuliah()->sum('sks');
            $item->mata_kuliah = $item->mata_kuliah()->get();
        }

        $data = [
            'all_mahasiswa' => $mahasiswa,
        ];

        return view('kelas', $data);
    }

    public function detailKelasMahasiswa(Request $request)
    {
        $mahasiswa = Mahasiswa::find($request->mahasiswa_id);
        $mahasiswa->total_sks = $mahasiswa->mata_kuliah()->sum('sks');

        $data = [
            'selected_mahasiswa' => $mahasiswa,
            'all_mata_kuliah' => $mahasiswa->mata_kuliah()->get(),
        ];

        return view('kelas-mahasiswa', $data);
    }

    public function showFormAdd(Request $request)
    {
        $mahasiswa = Mahasiswa::find($request->mahasiswa_id);
        $mahasiswa->total_sks = $mahasiswa->mata_kuliah()->sum('sks');

        $data = [
            'selected_mahasiswa' => $mahasiswa,
            'all_mata_kuliah' => MataKuliah::all(),
        ];

        return view('kelas-mahasiswa-form-add', $data);
    }

    public function add(KelasMahasiswaAddvalidation $request)
    {
        $data = $request->validated();

        Mahasiswa::find($data['mahasiswa_id'])->mata_kuliah()->sync([$data['mata_kuliah_id'] => ['nama' => $data['nama']]], false);

        return redirect('/kelas/mahasiswa/'.$data['mahasiswa_id'].'/mata-kuliah');
    }

    public function delete(Request $request)
    {
        Mahasiswa::find($request->mahasiswa_id)->mata_kuliah()->detach($request->mata_kuliah_id);

        return redirect('/kelas/mahasiswa/'.$request->mahasiswa_id.'/mata-kuliah');
    }
}
