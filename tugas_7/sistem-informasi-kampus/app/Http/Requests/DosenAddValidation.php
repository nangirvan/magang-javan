<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DosenAddValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|max:100|unique:dosen,nama',
            'nip' => 'required|numeric|unique:dosen,nip',
            'gelar' => 'required|max:100',
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute tidak boleh kosong',
            'max' => ':attribute tidak boleh lebih dari :max karakter',
            'numeric' => ':attribute harus nomor',
            'unique' => ':attribute sudah dipakai',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'nama' => ucwords(strtolower($this->nama)),
            'nip' => ucwords(strtolower($this->nip)),
            'gelar' => ucwords(strtolower($this->gelar)),
        ]);
    }
}
