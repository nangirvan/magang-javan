<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MataKuliahEditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric|exists:mata_kuliah,id',
            'nama' => 'required|max:100|unique:mata_kuliah,nama,'.$this->id,
            'sks' => 'required|numeric|max:24',
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute tidak boleh kosong',
            'exists' => 'Data tidak ditemukan',
            'nama.max' => ':attribute tidak boleh lebih dari :max karakter',
            'sks.max' => ':attribute tidak boleh lebih dari :max',
            'unique' => ':attribute sudah terdaftar',
            'numeric' => ':attribute tidak valid',
        ];
    }

    public function prepareForValidation()
    {
        return $this->merge([
            'nama' => ucwords(strtolower($this->nama)),
        ]);
    }
}
