<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MahasiswaAddValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|max:100|unique:mahasiswa,nama',
            'nim' => 'required|numeric|unique:mahasiswa,nim',
            'jenis_kelamin' => 'required|max:1|in:L,P',
            'tempat_lahir' => 'required|max:100',
            'tanggal_lahir' => 'required|date|after:1900-01-01',
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute tidak boleh kosong',
            'max' => ':attribute tidak boleh lebih dari :max karakter',
            'unique' => ':attribute sudah dipakai',
            'in' => ':attribute tidak valid',
            'date' => ':attribute tidak valid',
            'after' => ':attribute tidak valid',
        ];
    }

    public function prepareForValidation()
    {
        return $this->merge([
            'nama' => ucwords(strtolower($this->nama)),
            'tempat_lahir' => ucwords(strtolower($this->tempat_lahir)),
        ]);
    }
}
