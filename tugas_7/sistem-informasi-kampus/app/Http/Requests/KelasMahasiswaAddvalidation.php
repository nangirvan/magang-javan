<?php

namespace App\Http\Requests;

use App\Models\MataKuliah;
use Illuminate\Foundation\Http\FormRequest;

class KelasMahasiswaAddvalidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mahasiswa_id' => 'required|numeric|exists:mahasiswa,id',
            'mata_kuliah_id' => 'required|numeric|exists:mata_kuliah,id',
            'nama' => 'required|alpha|max:1',
            'total_sks' => 'required|numeric|max:24'
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute tidak boleh kosong',
            'numeric' => ':attribute tidak valid',
            'exists' => 'Data tidak ditemukan',
            'alpha' => ':attribute tidak valid',
            'nama.max' => ':attribute tidak boleh lebih dari :max karakter',
            'total_sks.max' => 'Total SKS tidak boleh lebih dari :max',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'nama' => strtoupper($this->nama),
            'total_sks' => ($this->total_sks) + (MataKuliah::find($this->mata_kuliah_id)->sks),
        ]);
    }
}
