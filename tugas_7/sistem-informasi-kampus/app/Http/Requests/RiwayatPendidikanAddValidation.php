<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RiwayatPendidikanAddValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dosen_id' => 'required|numeric|exists:dosen,id',
            'strata' => 'required|max:100',
            'jurusan' => 'required|max:100',
            'sekolah' => 'required|max:100',
            'tahun_mulai' => 'required|numeric|min:1900|max:2020',
            'tahun_selesai' => 'required|numeric|min:'.$this->tahun_mulai.'|max:2020',
        ];
    }

    public function messages()
    {
        return [
            'exists' => 'Data tidak ditemukan',
            'required' => ':attribute tidak boleh kosong',
            'numeric' => ':attribute harus nomor',
            'min' => ':attribute tidak valid',
            'max' => ':attribute tidak boleh lebih dari :max',
        ];
    }

    public function prepareForValidation()
    {
        return $this->merge([
            'strata' => strtoupper($this->strata),
            'jurusan' => ucwords(strtolower($this->jurusan)),
            'sekolah' => ucwords(strtolower($this->sekolah)),
        ]);
    }
}
