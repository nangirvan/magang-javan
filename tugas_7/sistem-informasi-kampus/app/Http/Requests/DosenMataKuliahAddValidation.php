<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DosenMataKuliahAddValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dosen_id' => 'required|numeric|exists:dosen,id',
            'mata_kuliah_id' => 'required|array|exists:mata_kuliah,id',
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute tidak boleh kosong',
            'numeric' => ':attribute tidak valid',
            'exists' => 'Data tidak ditemukan',
        ];
    }
}
