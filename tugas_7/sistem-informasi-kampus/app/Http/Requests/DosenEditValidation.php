<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DosenEditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric|exists:dosen,id',
            'nama' => 'required|max:100|unique:dosen,nama,'.$this->id,
            'nip' => 'required|numeric|unique:dosen,nip,'.$this->id,
            'gelar' => 'required|max:100',
        ];
    }

    public function messages()
    {
        return [
            'exists' => 'Data tidak ditemukan',
            'required' => ':attribute tidak boleh kosong',
            'max' => ':attribute tidak boleh lebih dari :max karakter',
            'numeric' => ':attribute harus nomor',
            'unique' => ':attribute sudah dipakai',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'nama' => ucwords(strtolower($this->nama)),
        ]);
    }
}
