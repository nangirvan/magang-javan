<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RiwayatPendidikan extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'riwayat_pendidikan';
    protected $fillable = ['dosen_id', 'strata', 'jurusan', 'sekolah', 'tahun_mulai', 'tahun_selesai'];

    public function dosen()
    {
        return $this->belongsTo('App\Models\Dosen');
    }
}
