<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Mahasiswa extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'mahasiswa';
    protected $fillable = ['nama', 'nim', 'jenis_kelamin', 'tempat_lahir', 'tanggal_lahir'];

    public function mata_kuliah()
    {
        return $this->belongsToMany('App\Models\MataKuliah', 'kelas', 'mahasiswa_id', 'mata_kuliah_id')->withPivot('nama');
    }

    public function getJenisKelaminAttribute($value)
    {
        return ($value == 'L') ? 'Laki-Laki' : 'Perempuan';
    }

    public function getTanggalLahirAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d', $value)->format('d M Y');
    }
}
