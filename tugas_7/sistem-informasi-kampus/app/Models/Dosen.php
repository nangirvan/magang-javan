<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'dosen';
    protected $fillable = ['nama', 'nip', 'gelar'];

    public function riwayat_pendidikan()
    {
        return $this->hasMany('App\Models\RiwayatPendidikan');
    }

    public function mata_kuliah()
    {
        return $this->belongsToMany('App\Models\MataKuliah', 'dosen_mata_kuliah', 'dosen_id', 'mata_kuliah_id');
    }
}
