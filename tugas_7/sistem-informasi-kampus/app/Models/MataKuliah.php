<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MataKuliah extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'mata_kuliah';
    protected $fillable = ['nama', 'sks'];

    public function dosen()
    {
        return $this->belongsToMany('App\Models\Dosen', 'dosen_mata_kuliah', 'mata_kuliah_id', 'dosen_id');
    }

    public function mahasiswa()
    {
        return $this->belongsToMany('App\Models\Mahasiswa', 'kelas', 'mata_kuliah_id', 'mahasiswa_id')->withPivot('nama');
    }
}
