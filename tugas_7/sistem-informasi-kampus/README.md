## SISTEM INFORMASI KAMPUS

Pada bagian home aplikasi dengan route "/" terdapat beberapa navigasi ke :
- CRUD Dosen dan Riwayat Pendidikan
- CRUD Mahasiswa
- CRUD Mata Kuliah
- Pendaftaran Mata Kuliah Mahasiswa


## 1. CRUD Dosen dan Riwayat Pendidikan

## Dosen

- Route utamanya "/dosen"
- Controllernya "DosenController"
- Database :
    - Migrationnya "create_dosen"
    - Seedernya "DosenSeeder"
    - Modelnya "Dosen"
- Views :
    - View utamanya "dosen"
    - Form tambah datanya "dosen-form-add"
    - Form edit datanya "dosen-form-edit"
- Form Request :
    - Validasi tambah data menggunakan class "DosenAddValidation"
    - Validasi edit data menggunakan class "DosenEditValidation"
    
## Riwayat Pendidikan
    
- Route utamanya "/riwayat-pendidikan/{dosen_id}"
- Controllernya "RiwayatPendidikanController"
- Database :
    - Migrationnya "create_riwayat_pendidikan"
    - Seedernya "RiwayatPendidikanSeeder"
    - Modelnya "RiwayatPendidikan"
- Views :
    - View utamanya "riwayat-pendidikan"
    - Form tambah datanya "riwayat-pendidikan-form-add"
    - Form edit datanya "riwayat-pendidikan-form-edit"
- Form Request :
    - Validasi tambah data menggunakan class "RiwayatPendidikanAddValidation"    
    - Validasi edit data menggunakan class "RiwayatPendidikanEditValidation"
    

## 2. CRUD Mahasiswa
    
- Route utamanya "/mahasiswa"
- Controllernya "MahasiswaController"
- Database :
    - Migrationnya "create_mahasiswa"
    - Seedernya "MahasiswaSeeder"
    - Modelnya "Mahasiswa"
- Views :
    - View utamanya "mahasiswa"
    - Form tambah datanya "mahasiswa-form-add"
    - Form edit datanya "mahasiswa-form-edit"
- Form Request :
    - Validasi tambah data menggunakan class "MahasiswaAddValidation"    
    - Validasi edit data menggunakan class "MahasiswaEditValidation"    

    
## 3. CRUD Mata Kuliah
    
- Route utamanya "/mata-kuliah"
- Controllernya "MataKuliahController"
- Database :
    - Migrationnya "create_mata_kuliah"
    - Seedernya "MataKuliahSeeder"
    - Modelnya "MataKuliah"
- Views :
    - View utamanya "mata-kuliah"
    - Form tambah datanya "mata-kuliah-form-add"
    - Form edit datanya "mata-kuliah-form-edit"
- Form Request :
    - Validasi tambah data menggunakan class "MataKuliahAddValidation"    
    - Validasi edit data menggunakan class "MataKuliahEditValidation"

    
## 4. Pendaftaran Mata Kuliah Mahasiswa    
- Route utamanya "/kelas"
- Controllernya "KelasController"
- Database :
    - Migrationnya "create_kelas"
    - Seedernya "KelasSeeder"
    - Tabel pivotnya "kelas"
    - Modelnya Mahasiswa dan MataKuliah dengan relasi many-to-many
- Views :
    - View utamanya "kelas"
    - View detail mata kuliah setiap mahasiswa adalah "kelas-mahasiswa"
    - Form pendaftaran mata kuliah setiap mahasiswa adalah "kelas-mahasiswa-form-add"
- Form Request :
    - Validasi tambah datanya menggunakan class "KelasMahasiswaAddValidation"
