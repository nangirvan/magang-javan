@extends('layouts.app')

@section('title', 'Riwayat Pendidikan')

@section('content')
    @if(session('success'))
        <div class="alert alert-warning alert-dismissible fade show my-4" role="alert">
            <strong>Success!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="my-4">
        <h3 class="float-left">Riwayat Pendidikan {{ $selected_dosen->nama }}</h3>
        <a href="/riwayat-pendidikan/{{ $selected_dosen->id }}/add" class="float-right btn btn-primary text-white mb-4 font-weight-bold">Add Riwayat Pendidikan</a>
        <a href="/dosen" class="float-right btn btn-secondary text-white mb-4 mr-2">Back to Dosen</a>
        <a href="/" class="float-right btn btn-secondary text-white mb-4 mx-2">Home</a>
    </div>

    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th>No</th>
            <th>Strata</th>
            <th>Jurusan</th>
            <th>Sekolah</th>
            <th>Tahun Mulai</th>
            <th>Tahun Selesai</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php $i = 1; ?>
        @foreach($all_riwayat_pendidikan as $riwayat_pendidikan)
            <tr>
                <td>{{ $i }}</td>
                <td>{{ $riwayat_pendidikan->strata }}</td>
                <td>{{ $riwayat_pendidikan->jurusan }}</td>
                <td>{{ $riwayat_pendidikan->sekolah }}</td>
                <td>{{ $riwayat_pendidikan->tahun_mulai }}</td>
                <td>{{ $riwayat_pendidikan->tahun_selesai }}</td>
                <td>
                    <a href="/riwayat-pendidikan/{{ $selected_dosen->id }}/edit/{{ $riwayat_pendidikan->id }}" class="btn btn-warning text-white font-italic">Edit</a>
                    <a href="/riwayat-pendidikan/{{  $selected_dosen->id }}/delete/{{ $riwayat_pendidikan->id }}" class="btn btn-danger text-white font-italic">Delete</a>
                </td>
            </tr>
            <?php $i++; ?>
        @endforeach
        </tbody>
    </table>
@endsection
