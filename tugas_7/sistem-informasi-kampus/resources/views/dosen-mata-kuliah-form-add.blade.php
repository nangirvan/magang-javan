@extends('layouts.app')

@section('title', 'Add Mata Kuliah')

@section('content')
    <h5 class="my-4 text-center">Form Add Mata Kuliah {{ $selected_dosen->nama }}</h5>
    <form action="/dosen/{{ $selected_dosen->id }}/mata-kuliah/add" method="post">
        @csrf
        <input type="hidden" name="dosen_id" value="{{ $selected_dosen->id }}">
        @foreach($all_mata_kuliah as $mata_kuliah)
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="{{ $mata_kuliah->id }}" name="mata_kuliah_id[]" id="checkboxMataKuliah{{ $mata_kuliah->id }}">
                <label class="form-check-label" for="checkboxMataKuliah{{ $mata_kuliah->id }}">
                    {{ $mata_kuliah->nama }}
                </label>
            </div>
        @endforeach
        @error('mata_kuliah')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Add Mata Kuliah</button>
            <a href="/dosen/{{ $selected_dosen->id }}/mata-kuliah" class="btn btn-danger float-right" type="submit">Cancel</a>
        </div>
    </form>
@endsection
