@extends('layouts.app')

@section('title', 'Edit Mata Kuliah')

@section('content')
    <h5 class="my-4 text-center">Form Edit</h5>
    <form action="/mata-kuliah/edit" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $selected_mata_kuliah->id }}">
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ $selected_mata_kuliah->nama }}" required>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inSKS">SKS</label>
            <input class="form-control @error('sks') is-invalid @enderror" type="number" name="sks" id="inSKS" value="{{ $selected_mata_kuliah->sks }}" required>
            @error('sks')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Submit</button>
            <a class="btn btn-danger float-right" href="/mata-kuliah">Cancel</a>
        </div>
    </form>
@endsection
