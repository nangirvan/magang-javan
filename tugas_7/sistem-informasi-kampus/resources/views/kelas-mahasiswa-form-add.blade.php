@extends('layouts.app')

@section('title', 'Add Mata Kuliah')

@section('content')
    <h5 class="my-4 text-center">Form Add Mata Kuliah {{ $selected_mahasiswa->nama }}</h5>
    <form action="/kelas/mahasiswa/{{ $selected_mahasiswa->id }}/mata-kuliah/add" method="post">
        @csrf
        <input type="hidden" name="mahasiswa_id" value="{{ $selected_mahasiswa->id }}">
        <input type="hidden" name="total_sks" value="{{ $selected_mahasiswa->total_sks }}">
        <div class="form-group">
            <label for="inMataKuliah">Mata Kuliah</label>
            <select class="form-control @error('mata_kuliah_id') is-invalid @enderror @error('total_sks') is-invalid @enderror" name="mata_kuliah_id" id="inMataKuliah" required>
                <option value="">Select ...</option>
                @foreach($all_mata_kuliah as $mata_kuliah)
                    <option value="{{ $mata_kuliah->id }}">{{ $mata_kuliah->nama }} ({{ $mata_kuliah->sks }} SKS)</option>
                @endforeach
            </select>
            @error('mata_kuliah_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
            @error('total_sks')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inNama">Nama Kelas</label>
            <select class="form-control @error('nama') is-invalid @enderror @error('nama') is-invalid @enderror" name="nama" id="inNama" required>
                <option value="">Select ...</option>
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
                <option value="D">D</option>
            </select>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Add Mata Kuliah</button>
            <a href="/kelas/mahasiswa/{{ $selected_mahasiswa->id }}/mata-kuliah" class="btn btn-danger float-right" type="submit">Cancel</a>
        </div>
    </form>
@endsection
