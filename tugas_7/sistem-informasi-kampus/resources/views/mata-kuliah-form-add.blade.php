@extends('layouts.app')

@section('title', 'Add Mata Kuliah')

@section('content')
    <h5 class="my-4 text-center">Form Add Mata Kuliah</h5>
    <form action="/mata-kuliah/add" method="post">
        @csrf
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ old('nama') }}" required>
            <small id="helpNama" class="form-text text-muted">Contoh : Pemrograman PHP</small>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inSKS">SKS</label>
            <input class="form-control @error('sks') is-invalid @enderror" type="number" name="sks" id="inSKS" value="{{ old('sks') }}" required>
            <small id="helpSKS" class="form-text text-muted">Contoh : 4</small>
            @error('sks')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Add Mata Kuliah</button>
            <a href="/mata-kuliah" class="btn btn-danger float-right" type="submit">Cancel</a>
        </div>
    </form>
@endsection
