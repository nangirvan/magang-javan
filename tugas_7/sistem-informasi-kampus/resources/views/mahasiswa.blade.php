@extends('layouts.app')

@section('title', 'Mahasiswa')

@section('content')
    @if(session('success'))
        <div class="alert alert-warning alert-dismissible fade show my-4" role="alert">
            <strong>Success!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="my-4">
        <h3 class="float-left">Mahasiswa</h3>
        <a href="/mahasiswa/add" class="float-right btn btn-primary text-white mb-4 font-weight-bold">Add Mahasiswa</a>
        <a href="/" class="float-right btn btn-secondary text-white mb-4 mx-2">Home</a>
    </div>

    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>NIM</th>
            <th>Jenis Kelamin</th>
            <th>Tempat, Tanggal Lahir</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php $i = 1; ?>
        @foreach($all_mahasiswa as $mahasiswa)
            <tr>
                <td>{{ $i }}</td>
                <td>{{ $mahasiswa->nama }}</td>
                <td>{{ $mahasiswa->nim }}</td>
                <td>{{ $mahasiswa->jenis_kelamin }}</td>
                <td>{{ $mahasiswa->tempat_lahir}}, {{ $mahasiswa->tanggal_lahir }}</td>
                <td>
                    <a href="/mahasiswa/edit/{{ $mahasiswa->id }}" class="btn btn-warning text-white font-italic">Edit</a>
                    <a href="/mahasiswa/delete/{{ $mahasiswa->id }}" class="btn btn-danger text-white font-italic">Delete</a>
                </td>
            </tr>
            <?php $i++; ?>
        @endforeach
        </tbody>
    </table>
@endsection
