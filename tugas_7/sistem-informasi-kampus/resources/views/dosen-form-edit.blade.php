@extends('layouts.app')

@section('title', 'Edit Dosen')

@section('content')
    <h5 class="my-4 text-center">Form Edit</h5>
    <form action="/dosen/edit" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $selected_dosen->id }}">
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ $selected_dosen->nama }}" required>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inNip">NIP</label>
            <input class="form-control @error('nip') is-invalid @enderror" type="number" name="nip" id="inNip" value="{{ $selected_dosen->nip }}" required>
            @error('nip')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inGelar">Gelar</label>
            <input class="form-control @error('gelar') is-invalid @enderror" type="text" name="gelar" id="inGelar" value="{{ $selected_dosen->gelar }}" required>
            @error('gelar')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Submit</button>
            <a class="btn btn-danger float-right" href="/dosen">Cancel</a>
        </div>
    </form>
@endsection
