@extends('layouts.app')

@section('title', 'Edit Mahasiswa')

@section('content')
    <h5 class="my-4 text-center">Form Edit</h5>
    <form action="/mahasiswa/edit" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $selected_mahasiswa->id }}">
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ $selected_mahasiswa->nama }}" required>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inNIM">NIM</label>
            <input class="form-control @error('nim') is-invalid @enderror" type="number" name="nim" id="inNIM" value="{{ $selected_mahasiswa->nim }}" required>
            @error('nim')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inJenisKelamin">Jenis Kelamin</label>
            <select class="form-control @error('jenis_kelamin') is-invalid @enderror" name="jenis_kelamin" id="inJenisKelamin" required>
                <option value="L" @if($selected_mahasiswa->jenis_kelamin == 'L') selected @endif>Laki-Laki</option>
                <option value="P" @if($selected_mahasiswa->jenis_kelamin == 'P') selected @endif>Perempuan</option>
            </select>
            @error('jenis_kelamin')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inTempatLahir">Tempat Lahir</label>
            <input class="form-control @error('tempat_lahir') is-invalid @enderror" type="text" name="tempat_lahir" id="inTempatLahir" value="{{ $selected_mahasiswa->tempat_lahir }}" required>
            @error('tempat_lahir')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inTanggalLahir">Tanggal Lahir</label>
            <input class="form-control @error('tanggal_lahir') is-invalid @enderror" type="date" name="tanggal_lahir" id="inTanggalLahir" value="{{ \Carbon\Carbon::parse($selected_mahasiswa->tanggal_lahir)->format('Y-m-d') }}" required>
            @error('tanggal_lahir')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Submit</button>
            <a class="btn btn-danger float-right" href="/mahasiswa">Cancel</a>
        </div>
    </form>
@endsection
