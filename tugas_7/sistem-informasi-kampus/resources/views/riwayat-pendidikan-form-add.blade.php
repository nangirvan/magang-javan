@extends('layouts.app')

@section('title', 'Add Riwayat Pendidikan')

@section('content')
    <h5 class="my-4 text-center">Form Add Riwayat Pendidikan</h5>
    <form action="/riwayat-pendidikan/add" method="post">
        @csrf
        <input type="hidden" name="dosen_id" value="{{ $selected_dosen->id }}">
        <div class="form-group">
            <label for="inStrata">Strata</label>
            <input class="form-control @error('strata') is-invalid @enderror" type="text" name="strata" id="inStrata" value="{{ old('strata') }}" required>
            <small id="helpStrata" class="form-text text-muted">Contoh : S2</small>
            @error('strata')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inJurusan">Jurusan</label>
            <input class="form-control @error('jurusan') is-invalid @enderror" type="text" name="jurusan" id="inJurusan" value="{{ old('jurusan') }}" required>
            <small id="helpJurusan" class="form-text text-muted">Contoh : Ilmu Komputer</small>
            @error('jurusan')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inSekolah">Sekolah</label>
            <input class="form-control @error('sekolah') is-invalid @enderror" type="text" name="sekolah" id="inSekolah" value="{{ old('sekolah') }}" required>
            <small id="helpSekolah" class="form-text text-muted">Contoh : Universitas Brawijaya</small>
            @error('sekolah')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inTahunMulai">Tahun Mulai</label>
            <input class="form-control @error('tahun_mulai') is-invalid @enderror" type="number" name="tahun_mulai" id="inTahunMulai" value="{{ old('tahun_mulai') }}" min="1900" max="2020" required>
            <small id="helpTahunMulai" class="form-text text-muted">Contoh : 2016</small>
            @error('tahun_mulai')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inTahunSelesai">Tahun Selesai</label>
            <input class="form-control @error('tahun_selesai') is-invalid @enderror" type="number" name="tahun_selesai" id="inTahunSelesai" value="{{ old('tahun_selesai') }}" min="1900" max="2020" required>
            <small id="helpTahunSelesai" class="form-text text-muted">Contoh : 2018</small>
            @error('tahun_selesai')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Add Riwayat Pendidikan</button>
            <a href="/riwayat-pendidikan/{{ $selected_dosen->id }}" class="btn btn-danger float-right" type="submit">Cancel</a>
        </div>
    </form>
@endsection
