@extends('layouts.app')

@section('title', 'Dosen')

@section('content')
    @if(session('success'))
        <div class="alert alert-warning alert-dismissible fade show my-4" role="alert">
            <strong>Success!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="my-4">
        <h3 class="float-left">Dosen</h3>
        <a href="/dosen/add" class="float-right btn btn-primary text-white mb-4 font-weight-bold">Add Dosen</a>
        <a href="/" class="float-right btn btn-secondary text-white mb-4 mx-2">Home</a>
    </div>

    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>NIP</th>
            <th>Gelar</th>
            <th>Riwayat Pendidikan</th>
            <th>Mata Kuliah yang Diajarkan</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($all_dosen as $dosen)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $dosen->nama }}</td>
                <td>{{ $dosen->nip }}</td>
                <td>{{ $dosen->gelar }}</td>
                <td>
                    <ul class="list-group">
                        @foreach($dosen->riwayat_pendidikan as $riwayat_pendidikan)
                            <li class="list-group-item text-center" style="font-size: 11px;">{{ $riwayat_pendidikan->strata }} - {{ $riwayat_pendidikan->jurusan }}</li>
                        @endforeach
                    </ul>
                </td>
                <td>
                    <ul class="list-group">
                        @foreach($dosen->mata_kuliah as $mata_kuliah)
                            <li class="list-group-item text-center" style="font-size: 11px;">{{ $mata_kuliah->nama }}</li>
                        @endforeach
                    </ul>
                </td>
                <td>
                    <a href="/dosen/edit/{{ $dosen->id }}" class="btn btn-block btn-warning text-white font-italic my-1">Edit</a>
                    <a href="/dosen/delete/{{ $dosen->id }}" class="btn btn-block btn-danger text-white font-italic my-1">Delete</a>
                    <a href="/dosen/{{ $dosen->id }}/mata-kuliah" class="btn btn-block btn-primary text-white font-italic my-1">Detail Mata Kuliah</a>
                    <a href="/riwayat-pendidikan/{{ $dosen->id }}" class="btn btn-block btn-primary text-white font-italic my-1">Detail Riwayat Pendidikan</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
