@extends('layouts.app')

@section('title', 'Add Dosen')

@section('content')
    <h5 class="my-4 text-center">Form Add Dosen</h5>
    <form action="/dosen/add" method="post">
        @csrf
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ old('nama') }}" required>
            <small id="helpNama" class="form-text text-muted">Contoh : Anto</small>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inNip">NIP</label>
            <input class="form-control @error('nip') is-invalid @enderror" type="number" name="nip" id="inNip" value="{{ old('nip') }}" required>
            <small id="helpNip" class="form-text text-muted">Contoh : 199906022005</small>
            @error('nip')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inGelar">Gelar</label>
            <input class="form-control @error('gelar') is-invalid @enderror" type="text" name="gelar" id="inGelar" value="{{ old('gelar') }}" required>
            <small id="helpGelar" class="form-text text-muted">Contoh : S.Kom, M.Kom</small>
            @error('gelar')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Add Dosen</button>
            <a href="/dosen" class="btn btn-danger float-right" type="submit">Cancel</a>
        </div>
    </form>
@endsection
