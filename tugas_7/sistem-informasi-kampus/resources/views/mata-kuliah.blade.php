@extends('layouts.app')

@section('title', 'Mata Kuliah')

@section('content')
    @if(session('success'))
        <div class="alert alert-warning alert-dismissible fade show my-4" role="alert">
            <strong>Success!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="my-4">
        <h3 class="float-left">Mata Kuliah</h3>
        <a href="/mata-kuliah/add" class="float-right btn btn-primary text-white mb-4 font-weight-bold">Add Mata Kuliah</a>
        <a href="/" class="float-right btn btn-secondary text-white mb-4 mx-2">Home</a>
    </div>

    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>SKS</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($all_mata_kuliah as $mata_kuliah)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $mata_kuliah->nama }}</td>
                <td>{{ $mata_kuliah->sks }}</td>
                <td>
                    <a href="/mata-kuliah/edit/{{ $mata_kuliah->id }}" class="btn btn-warning text-white font-italic">Edit</a>
                    <a href="/mata-kuliah/delete/{{ $mata_kuliah->id }}" class="btn btn-danger text-white font-italic">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
