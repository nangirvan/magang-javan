@extends('layouts.app')

@section('title', 'Mahasiswa')

@section('content')
    @if(session('success'))
        <div class="alert alert-warning alert-dismissible fade show my-4" role="alert">
            <strong>Success!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="my-4">
        <h3 class="float-left">Kelas</h3>
        <a href="/" class="float-right btn btn-secondary text-white mb-4 mx-2">Home</a>
    </div>

    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>NIM</th>
            <th>Total SKS</th>
            <th>Mata Kuliah</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php $i = 1; ?>
        @foreach($all_mahasiswa as $mahasiswa)
            <tr>
                <td>{{ $i }}</td>
                <td>{{ $mahasiswa->nama }}</td>
                <td>{{ $mahasiswa->nim }}</td>
                <td>{{ $mahasiswa->total_sks }}</td>
                <td>
                    <ul class="list-group">
                        @foreach($mahasiswa->mata_kuliah as $mata_kuliah)
                            <li class="list-group-item text-center" style="font-size: 11px;">
                                <span class="float-left">
                                    {{ $mata_kuliah->nama }}
                                </span>
                                <span class="float-right">
                                {{ $mata_kuliah->pivot->nama }}
                                </span>
                            </li>
                        @endforeach
                    </ul>
                </td>
                <td>
                    <a href="/kelas/mahasiswa/{{ $mahasiswa->id }}/mata-kuliah" class="btn btn-block btn-primary text-white font-italic">Detail Mata Kuliah</a>
                </td>
            </tr>
            <?php $i++; ?>
        @endforeach
        </tbody>
    </table>
@endsection
