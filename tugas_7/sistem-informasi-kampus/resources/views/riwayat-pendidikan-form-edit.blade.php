@extends('layouts.app')

@section('title', 'Edit Riwayat Pendidikan')

@section('content')
    <h5 class="my-4 text-center">Form Edit</h5>
    <form action="/riwayat-pendidikan/edit" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $selected_riwayat_pendidikan->id }}">
        <input type="hidden" name="dosen_id" value="{{ $selected_riwayat_pendidikan->dosen_id }}">
        <div class="form-group">
            <label for="inStrata">Strata</label>
            <input class="form-control @error('strata') is-invalid @enderror" type="text" name="strata" id="inStrata" value="{{ $selected_riwayat_pendidikan->strata }}" required>
            @error('strata')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inJurusan">Jurusan</label>
            <input class="form-control @error('jurusan') is-invalid @enderror" type="text" name="jurusan" id="inJurusan" value="{{ $selected_riwayat_pendidikan->jurusan }}" required>
            @error('jurusan')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inSekolah">Sekolah</label>
            <input class="form-control @error('sekolah') is-invalid @enderror" type="text" name="sekolah" id="inSekolah" value="{{ $selected_riwayat_pendidikan->sekolah }}" required>
            @error('sekolah')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inTahunMulai">Tahun Mulai</label>
            <input class="form-control @error('tahun_mulai') is-invalid @enderror" type="number" name="tahun_mulai" id="inTahunMulai" value="{{ $selected_riwayat_pendidikan->tahun_mulai }}" max="2020" required>
            @error('tahun_mulai')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inTahunSelesai">Tahun Selesai</label>
            <input class="form-control @error('tahun_selesai') is-invalid @enderror" type="number" name="tahun_selesai" id="inTahunSelesai" value="{{ $selected_riwayat_pendidikan->tahun_selesai }}" max="2020" required>
            @error('tahun_selesai')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Submit</button>
            <a class="btn btn-danger float-right" href="/riwayat-pendidikan/{{ $selected_riwayat_pendidikan->dosen_id }}">Cancel</a>
        </div>
    </form>
@endsection
