@extends('layouts.app')

@section('title', 'Home')

@section('content')

    <div class="text-center my-4">
        <h2 class="my-4">Sistem Informasi Kampus</h2>
        <hr style="width: 75%">
        <a class="btn btn-lg btn-primary my-2 mx-2" href="/dosen">CRUD Dosen dan Riwayat Pendidikan</a>
        <a class="btn btn-lg btn-primary my-2 mx-2" href="/mahasiswa">CRUD Mahasiswa</a>
        <a class="btn btn-lg btn-primary my-2 mx-2" href="/mata-kuliah">CRUD Mata Kuliah</a>
    </div>
    <div class="text-center my-4">
        <hr style="width: 75%">
        <a class="btn btn-lg btn-primary my-2 mx-2" href="/kelas">Pendaftaran Mata Kuliah</a>
    </div>
@endsection

