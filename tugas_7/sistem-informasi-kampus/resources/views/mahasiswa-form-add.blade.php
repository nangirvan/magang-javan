@extends('layouts.app')

@section('title', 'Add Mahasiswa')

@section('content')
    <h5 class="my-4 text-center">Form Add Mahasiswa</h5>
    <form action="/mahasiswa/add" method="post">
        @csrf
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ old('nama') }}" required>
            <small id="helpNama" class="form-text text-muted">Contoh : Anto</small>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inNIM">NIM</label>
            <input class="form-control @error('nim') is-invalid @enderror" type="number" name="nim" id="inNIM" value="{{ old('nim') }}" required>
            <small id="helpNIM" class="form-text text-muted">Contoh : 199812310001</small>
            @error('nim')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inJenisKelamin">JenisKelamin</label>
            <select class="form-control @error('jenis_kelamin') is-invalid @enderror" name="jenis_kelamin" id="inJenisKelamin" required>
                <option value="">Select ...</option>
                <option value="L">Laki-Laki</option>
                <option value="P">Perempuan</option>
            </select>
            @error('jenis_kelamin')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inTempatLahir">Tempat Lahir</label>
            <input class="form-control @error('tempat_lahir') is-invalid @enderror" type="text" name="tempat_lahir" id="inTempatLahir" value="{{ old('tempat_lahir') }}" required>
            <small id="helpTempatLahir" class="form-text text-muted">Contoh : Malang</small>
            @error('tempat_lahir')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inTanggalLahir">Tanggal Lahir</label>
            <input class="form-control @error('tanggal_lahir') is-invalid @enderror" type="date" name="tanggal_lahir" id="inTanggalLahir" value="{{ old('tanggal_lahir') }}" required>
            @error('tanggal_lahir')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Add Mahasiswa</button>
            <a href="/mahasiswa" class="btn btn-danger float-right" type="submit">Cancel</a>
        </div>
    </form>
@endsection
