<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

// CRUD Dosen
Route::get('/dosen', 'DosenController@index');
Route::get('/dosen/add', 'DosenController@showFormAdd');
Route::get('/dosen/edit/{id}', 'DosenController@showFormEdit');
Route::get('/dosen/delete/{id}', 'DosenController@delete');
Route::post('/dosen/add', 'DosenController@add');
Route::post('/dosen/edit', 'DosenController@edit');

// CRUD Riwayat Pendidikan
Route::get('/riwayat-pendidikan/{dosen_id}', 'RiwayatPendidikanController@index');
Route::get('/riwayat-pendidikan/{dosen_id}/add/', 'RiwayatPendidikanController@showFormAdd');
Route::get('/riwayat-pendidikan/{dosen_id}/edit/{id}', 'RiwayatPendidikanController@showFormEdit');
Route::get('/riwayat-pendidikan/{dosen_id}/delete/{id}', 'RiwayatPendidikanController@delete');
Route::post('/riwayat-pendidikan/add', 'RiwayatPendidikanController@add');
Route::post('/riwayat-pendidikan/edit', 'RiwayatPendidikanController@edit');

// CRUD Mahasiswa
Route::get('/mahasiswa', 'MahasiswaController@index');
Route::get('/mahasiswa/add', 'MahasiswaController@showFormAdd');
Route::get('/mahasiswa/edit/{id}', 'MahasiswaController@showFormEdit');
Route::get('/mahasiswa/delete/{id}', 'MahasiswaController@delete');
Route::post('/mahasiswa/add', 'MahasiswaController@add');
Route::post('/mahasiswa/edit', 'MahasiswaController@edit');

// CRUD Mata Kuliah
Route::get('/mata-kuliah', 'MataKuliahController@index');
Route::get('/mata-kuliah/add', 'MataKuliahController@showFormAdd');
Route::get('/mata-kuliah/edit/{id}', 'MataKuliahController@showFormEdit');
Route::get('/mata-kuliah/delete/{id}', 'MataKuliahController@delete');
Route::post('/mata-kuliah/add', 'MataKuliahController@add');
Route::post('/mata-kuliah/edit', 'MataKuliahController@edit');

// Many-to-Many Dosen dan Mata Kuliah
Route::get('/dosen/{dosen_id}/mata-kuliah', 'DosenMataKuliahController@index');
Route::get('/dosen/{dosen_id}/mata-kuliah/add/', 'DosenMataKuliahController@showFormAdd');
Route::get('/dosen/{dosen_id}/mata-kuliah/delete/{mata_kuliah_id}', 'DosenMataKuliahController@delete');
Route::post('/dosen/{dosen_id}/mata-kuliah/add', 'DosenMataKuliahController@add');

// CRUD Kelas
Route::get('/kelas', 'KelasController@index');
Route::get('/kelas/mahasiswa/{mahasiswa_id}/mata-kuliah', 'KelasController@detailKelasMahasiswa');
Route::get('/kelas/mahasiswa/{mahasiswa_id}/mata-kuliah/add/', 'KelasController@showFormAdd');
Route::get('/kelas/mahasiswa/{mahasiswa_id}/mata-kuliah/delete/{mata_kuliah_id}', 'KelasController@delete');
Route::post('/kelas/mahasiswa/{mahasiswa_id}/mata-kuliah/add', 'KelasController@add');
