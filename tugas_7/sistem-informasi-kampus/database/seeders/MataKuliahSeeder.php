<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MataKuliahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mata_kuliah')->insert([
            ['nama' => 'Pemrograman Dasar', 'sks' => 4],
            ['nama' => 'Algoritma Struktur Data', 'sks' => 4],
            ['nama' => 'Sistem Basis Data', 'sks' => 4],
            ['nama' => 'Jaringan Komputer', 'sks' => 4],
            ['nama' => 'Arsitektur Organisasi Komputer', 'sks' => 4],
            ['nama' => 'Pemrograman Web', 'sks' => 3],
            ['nama' => 'Pemrograman Mobile', 'sks' => 3],
            ['nama' => 'Administrasi Basis Data', 'sks' => 3],
        ]);
    }
}
