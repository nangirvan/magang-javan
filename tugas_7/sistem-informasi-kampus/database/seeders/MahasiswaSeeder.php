<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mahasiswa')->insert([
            ['nama' => 'Danang', 'nim' => '1612345678', 'jenis_kelamin' => 'L', 'tempat_lahir' => 'Banjarnegara', 'tanggal_lahir' => '1998-06-02'],
            ['nama' => 'Anto', 'nim' => '1512345678', 'jenis_kelamin' => 'L', 'tempat_lahir' => 'Malang', 'tanggal_lahir' => '1997-12-31'],
            ['nama' => 'Putri', 'nim' => '1556781234', 'jenis_kelamin' => 'P', 'tempat_lahir' => 'Jakarta', 'tanggal_lahir' => '1997-01-01'],
        ]);
    }
}
