<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kelas')->insert([
            ['mahasiswa_id' => 1, 'mata_kuliah_id' => 2, 'nama' => 'H'],
            ['mahasiswa_id' => 1, 'mata_kuliah_id' => 3, 'nama' => 'A'],
            ['mahasiswa_id' => 2, 'mata_kuliah_id' => 6, 'nama' => 'C'],
            ['mahasiswa_id' => 3, 'mata_kuliah_id' => 6, 'nama' => 'C'],
        ]);
    }
}
