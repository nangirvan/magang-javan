<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DosenMataKuliahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dosen_mata_kuliah')->insert([
            ['dosen_id' => 1, 'mata_kuliah_id' => 4],
            ['dosen_id' => 1, 'mata_kuliah_id' => 5],
            ['dosen_id' => 2, 'mata_kuliah_id' => 1],
            ['dosen_id' => 3, 'mata_kuliah_id' => 1],
        ]);
    }
}
