<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RiwayatPendidikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('riwayat_pendidikan')->insert([
            ['dosen_id' => 1, 'strata' => 'S1', 'jurusan' => 'Teknik Informatika', 'sekolah' => 'Institut Teknologi Sepuluh November', 'tahun_mulai' => 1990, 'tahun_selesai' => 1994],
            ['dosen_id' => 1, 'strata' => 'S2', 'jurusan' => 'Ilmu Komputer', 'sekolah' => 'Universitas Indonesia', 'tahun_mulai' => 1995, 'tahun_selesai' => 1998],
            ['dosen_id' => 2, 'strata' => 'S2', 'jurusan' => 'Ilmu Komputer', 'sekolah' => 'Universitas Brawijaya', 'tahun_mulai' => 2005, 'tahun_selesai' => 2007],
            ['dosen_id' => 3, 'strata' => 'S2', 'jurusan' => 'Ilmu Komputer', 'sekolah' => 'Institut Teknologi Bandung', 'tahun_mulai' => 2010, 'tahun_selesai' => 2012],
        ]);
    }
}
