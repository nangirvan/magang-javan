<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DosenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dosen')->insert([
            ['nama' => 'Pak Budi', 'nip' => '197012345678', 'gelar' => 'S.T, M.Kom'],
            ['nama' => 'Pak Tono', 'nip' => '198087654321', 'gelar' => 'S.Kom, M.Kom'],
            ['nama' => 'Bu Sinta', 'nip' => '198556781234', 'gelar' => 'S.Kom, M.Kom'],
        ]);
    }
}
