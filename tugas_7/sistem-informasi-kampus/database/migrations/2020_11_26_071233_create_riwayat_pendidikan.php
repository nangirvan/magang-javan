<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiwayatPendidikan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_pendidikan', function (Blueprint $table) {
            $table->id();
            $table->foreignId('dosen_id')
                  ->constrained('dosen')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->string('strata');
            $table->string('jurusan');
            $table->string('sekolah');
            $table->year('tahun_mulai');
            $table->year('tahun_selesai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_pendidikan');
    }
}
