<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProvinsiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provinsi')->insert(
            [
                ['nama' => 'jawa barat'],
                ['nama' => 'jawa tengah'],
                ['nama' => 'jawa timur'],
            ]
        );
    }
}
