<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KabupatenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kabupaten')->insert(
            [
                ['nama' => 'bandung', 'provinsi_id' => 1],
                ['nama' => 'banjarnegara', 'provinsi_id' => 2],
                ['nama' => 'malang', 'provinsi_id' => 3],
            ]
        );
    }
}
