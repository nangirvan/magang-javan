<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KecamatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kecamatan')->insert(
            [
                ['nama' => 'banjarnegara', 'kabupaten_id' => 2],
                ['nama' => 'bawang', 'kabupaten_id' => 2],
                ['nama' => 'sigaluh', 'kabupaten_id' => 2],
            ]
        );
    }
}
