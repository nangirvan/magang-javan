<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DesaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('desa')->insert(
            [
                ['nama' => 'argasoka', 'kecamatan_id' => 1],
                ['nama' => 'krandegan', 'kecamatan_id' => 1],
                ['nama' => 'sokanandi', 'kecamatan_id' => 1],
            ]
        );
    }
}
