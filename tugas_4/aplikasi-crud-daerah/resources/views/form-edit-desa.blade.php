@extends('layouts.app')

@section('title', 'Edit Desa')

@section('content')
    <h5 class="my-4 text-center">Form Edit</h5>
    <form action="/edit-desa" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $selected_desa[0]->id }}">
        <div class="form-group">
            <label for="inKecamatan">Kecamatan</label>
            <select class="form-control @error('kecamatan_id') is-invalid @enderror" id="inKecamatan" name="kecamatan_id">
                @foreach($all_kecamatan as $kecamatan)
                    @if($kecamatan->id === $selected_desa[0]->kecamatan_id)
                        <option value="{{ $kecamatan->id }}" selected>{{ $kecamatan->nama }}</option>
                    @else
                        <option value="{{ $kecamatan->id }}">{{ $kecamatan->nama }}</option>
                    @endif
                @endforeach
            </select>
            @error('kecamatan_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ $selected_desa[0]->nama }}" required>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Submit</button>
            <a class="btn btn-danger float-right" href="/desa">Cancel</a>
        </div>
    </form>
@endsection
