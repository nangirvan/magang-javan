@extends('layouts.app')

@section('title', 'Edit Provinsi')

@section('content')
    <h5 class="my-4 text-center">Form Edit</h5>
    <form action="/edit-provinsi" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $selected_provinsi[0]->id }}">
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ $selected_provinsi[0]->nama }}" required>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Submit</button>
            <a class="btn btn-danger float-right" href="/provinsi">Cancel</a>
        </div>
    </form>
@endsection
