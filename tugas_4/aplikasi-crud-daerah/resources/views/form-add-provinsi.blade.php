@extends('layouts.app')

@section('title', 'Add Provinsi')

@section('content')
    <h5 class="my-4 text-center">Form Add Provinsi</h5>
    <form action="/add-provinsi" method="post">
        @csrf
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ old('nama') }}" required>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Add Provinsi</button>
            <a href="/provinsi" class="btn btn-danger float-right" type="submit">Cancel</a>
        </div>
    </form>
@endsection
