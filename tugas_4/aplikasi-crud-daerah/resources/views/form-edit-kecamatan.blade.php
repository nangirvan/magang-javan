@extends('layouts.app')

@section('title', 'Edit Kecamatan')

@section('content')
    <h5 class="my-4 text-center">Form Edit</h5>
    <form action="/edit-kecamatan" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $selected_kecamatan[0]->id }}">
        <div class="form-group">
            <label for="inKabupaten">Kabupaten</label>
            <select class="form-control @error('kabupaten_id') is-invalid @enderror" id="inKabupaten" name="kabupaten_id">
                @foreach($all_kabupaten as $kabupaten)
                    @if($kabupaten->id === $selected_kecamatan[0]->kabupaten_id)
                        <option value="{{ $kabupaten->id }}" selected>{{ $kabupaten->nama }}</option>
                    @else
                        <option value="{{ $kabupaten->id }}">{{ $kabupaten->nama }}</option>
                    @endif
                @endforeach
            </select>
            @error('kabupaten_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ $selected_kecamatan[0]->nama }}" required>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Submit</button>
            <a class="btn btn-danger float-right" href="/kecamatan">Cancel</a>
        </div>
    </form>
@endsection
