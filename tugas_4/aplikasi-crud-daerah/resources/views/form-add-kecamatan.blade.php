@extends('layouts.app')

@section('title', 'Add Kecamatan')

@section('content')
    <h5 class="my-4 text-center">Form Add Kecamatan</h5>
    <form action="/add-kecamatan" method="post">
        @csrf
        <div class="form-group">
            <label for="inKabupaten">Kabupaten</label>
            <select class="form-control @error('kabupaten_id') is-invalid @enderror" name="kabupaten_id" id="inKabupaten" required>
                <option value="">Select kabupaten ...</option>
                @foreach($all_kabupaten as $kabupaten)
                    <option value="{{ $kabupaten->id }}">{{ $kabupaten->nama }}</option>
                @endforeach
            </select>
            @error('kabupaten_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ old('nama') }}" required>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Add Kecamatan</button>
            <a href="/kecamatan" class="btn btn-danger float-right" type="submit">Cancel</a>
        </div>
    </form>
@endsection
