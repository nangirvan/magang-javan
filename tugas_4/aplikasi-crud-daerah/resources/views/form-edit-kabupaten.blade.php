@extends('layouts.app')

@section('title', 'Edit Kabupaten')

@section('content')
    <h5 class="my-4 text-center">Form Edit</h5>
    <form action="/edit-kabupaten" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $selected_kabupaten[0]->id }}">
        <div class="form-group">
            <label for="inProvinsi">Provinsi</label>
            <select class="form-control @error('provinsi_id') is-invalid @enderror" id="inProvinsi" name="provinsi_id">
                @foreach($all_provinsi as $provinsi)
                    @if($provinsi->id === $selected_kabupaten[0]->provinsi_id)
                        <option value="{{ $provinsi->id }}" selected>{{ $provinsi->nama }}</option>
                    @else
                        <option value="{{ $provinsi->id }}">{{ $provinsi->nama }}</option>
                    @endif
                @endforeach
            </select>
            @error('provinsi_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ $selected_kabupaten[0]->nama }}" required>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Submit</button>
            <a class="btn btn-danger float-right" href="/kabupaten">Cancel</a>
        </div>
    </form>
@endsection
