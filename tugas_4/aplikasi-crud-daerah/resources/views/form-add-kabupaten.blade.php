@extends('layouts.app')

@section('title', 'Add Kabupaten')

@section('content')
    <h5 class="my-4 text-center">Form Add Kabupaten</h5>
    <form action="/add-kabupaten" method="post">
        @csrf
        <div class="form-group">
            <label for="inProvinsi">Provinsi</label>
            <select class="form-control @error('provinsi_id') is-invalid @enderror" name="provinsi_id" id="inProvinsi" required>
                <option value="">Select provinsi ...</option>
                @foreach($all_provinsi as $provinsi)
                    <option value="{{ $provinsi->id }}">{{ $provinsi->nama }}</option>
                @endforeach
            </select>
            @error('provinsi_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ old('nama') }}" required>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Add Kabupaten</button>
            <a href="/kabupaten" class="btn btn-danger float-right" type="submit">Cancel</a>
        </div>
    </form>
@endsection
