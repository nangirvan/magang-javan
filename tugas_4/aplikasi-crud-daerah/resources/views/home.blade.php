@extends('layouts.app')

@section('title', 'Home')

@section('content')

    <div class="text-center my-4">
        <h2 class="my-4">Aplikasi CRUD Daerah</h2>
        <hr style="width: 75%">
        <a class="btn btn-lg btn-primary my-2 mx-2" href="/provinsi">CRUD Provinsi</a>
        <a class="btn btn-lg btn-primary my-2 mx-2" href="/kabupaten">CRUD Kabupaten</a>
        <a class="btn btn-lg btn-primary my-2 mx-2" href="/kecamatan">CRUD Kecamatan</a>
        <a class="btn btn-lg btn-primary my-2 mx-2" href="/desa">CRUD Desa</a>
    </div>
@endsection

