@extends('layouts.app')

@section('title', 'Desa')

@section('content')
    @if(session('success'))
        <div class="alert alert-warning alert-dismissible fade show my-4" role="alert">
            <strong>Success!</strong> {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="my-4">
        <h3 class="float-left">Desa</h3>
        <a href="/add-desa" class="float-right btn btn-primary text-white mb-4 font-weight-bold">Add Desa</a>
        <a href="/" class="float-right btn btn-secondary text-white mb-4 mx-2">Home</a>
    </div>

    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th>Id</th>
            <th>Nama</th>
            <th>Kecamatan ID</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($all_desa as $desa)
            <tr>
                <td>{{ $desa->id }}</td>
                <td>{{ $desa->nama }}</td>
                <td>{{ $desa->kecamatan_id }}</td>
                <td>
                    <a href="/edit-desa/{{ $desa->id }}" class="btn btn-warning text-white font-italic">Edit</a>
                    <a href="/delete-desa/{{ $desa->id }}" class="btn btn-danger text-white font-italic">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
