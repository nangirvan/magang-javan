@extends('layouts.app')

@section('title', 'Add Desa')

@section('content')
    <h5 class="my-4 text-center">Form Add Desa</h5>
    <form action="/add-desa" method="post">
        @csrf
        <div class="form-group">
            <label for="inKecamatan">Kecamatan</label>
            <select class="form-control @error('kecamatan_id') is-invalid @enderror" name="kecamatan_id" id="inKecamatan" required>
                <option value="">Select kecamatan ...</option>
                @foreach($all_kecamatan as $kecamatan)
                    <option value="{{ $kecamatan->id }}">{{ $kecamatan->nama }}</option>
                @endforeach
            </select>
            @error('kecamatan_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ old('nama') }}" required>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Add Desa</button>
            <a href="/desa" class="btn btn-danger float-right" type="submit">Cancel</a>
        </div>
    </form>
@endsection
