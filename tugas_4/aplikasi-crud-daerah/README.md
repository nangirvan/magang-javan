## Aplikasi CRUD Daerah

Route "/" berisi navigasi ke 4 aplikasi berikut :
- CRUD Provinsi
- CRUD Kabupaten
- CRUD Kecamatan
- CRUD Desa


## CRUD Provinsi

- Route utama yang digunakan adalah "/provinsi"
- Controller nya adalah "ProvinsiController"
- Tentang database :
    - Model nya adalah "Provinsi"
    - Migration nya adalah "provinsi"
    - Seeder nya adalah "ProvinsiSeeder"
- Tentang views :
    - View utamanya adalah "provinsi"
    - View untuk tambah provinsi adalah "form-add-provinsi"
    - View untuk edit provinsi adalah "form-edit-provinsi"
- Tentang validation :
    - Validasi tambah provinsi menggunakan validator manual
    - Validasi edit provinsi menggunakan validator manual
    
    
## CRUD Kabupaten

- Route utama yang digunakan adalah "/kabupaten"
- Controller nya adalah "KabupatenController"
- Tentang database :
    - Model nya adalah "Kabupaten"
    - Migration nya adalah "kabupaten"
    - Seeder nya adalah "KabupatenSeeder"
- Tentang views :
    - View utamanya adalah "kabupaten"
    - View untuk tambah kabupaten adalah "form-add-kabupaten"
    - View untuk edit kabupaten adalah "form-edit-kabupaten"
- Tentang validation :
    - Validasi tambah kabupaten menggunakan validator manual
    - Validasi edit kabupaten menggunakan validator manual
    

## CRUD Kecamatan

- Route utama yang digunakan adalah "/kecamatan"
- Controller nya adalah "KecamatanController"
- Tentang database :
    - Model nya adalah "Kecamatan"
    - Migration nya adalah "kecamatan"
    - Seeder nya adalah "KecamatanSeeder"
- Tentang views :
    - View utamanya adalah "kecamatan"
    - View untuk tambah kecamatan adalah "form-add-kecamatan"
    - View untuk edit kecamatan adalah "form-edit-kecamatan"
- Tentang validation :
    - Validasi tambah kecamatan menggunakan validator manual
    - Validasi edit kecamatan menggunakan validator manual


## CRUD Desa

- Route utama yang digunakan adalah "/desa"
- Controller nya adalah "DesaController"
- Tentang database :
    - Model nya adalah "Desa"
    - Migration nya adalah "desa"
    - Seeder nya adalah "DesaSeeder"
- Tentang views :
    - View utamanya adalah "desa"
    - View untuk tambah desa adalah "form-add-desa"
    - View untuk edit desa adalah "form-edit-desa"
- Tentang validation :
    - Validasi tambah desa menggunakan validator manual
    - Validasi edit desa menggunakan validator manual
