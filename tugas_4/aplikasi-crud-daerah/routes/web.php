<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

// Provinsi
Route::get('/provinsi', 'ProvinsiController@getAllProvinsi');
Route::get('/add-provinsi', 'ProvinsiController@showFormAdd');
Route::get('/edit-provinsi/{id}', 'ProvinsiController@showFormEdit');
Route::get('/delete-provinsi/{id}', 'ProvinsiController@deleteProvinsi');
Route::post('/add-provinsi', 'ProvinsiController@addProvinsi');
Route::post('/edit-provinsi', 'ProvinsiController@editProvinsi');

// Kabupaten
Route::get('/kabupaten', 'KabupatenController@getAllKabupaten');
Route::get('/add-kabupaten', 'KabupatenController@showFormAdd');
Route::get('/edit-kabupaten/{id}', 'KabupatenController@showFormEdit');
Route::get('/delete-kabupaten/{id}', 'KabupatenController@deleteKabupaten');
Route::post('/add-kabupaten', 'KabupatenController@addKabupaten');
Route::post('/edit-kabupaten', 'KabupatenController@editKabupaten');

// Kecamatan
Route::get('/kecamatan', 'KecamatanController@getAllKecamatan');
Route::get('/add-kecamatan', 'KecamatanController@showFormAdd');
Route::get('/edit-kecamatan/{id}', 'KecamatanController@showFormEdit');
Route::get('/delete-kecamatan/{id}', 'KecamatanController@deleteKecamatan');
Route::post('/add-kecamatan', 'KecamatanController@addKecamatan');
Route::post('/edit-kecamatan', 'KecamatanController@editKecamatan');

// Desa
Route::get('/desa', 'DesaController@getAllDesa');
Route::get('/add-desa', 'DesaController@showFormAdd');
Route::get('/edit-desa/{id}', 'DesaController@showFormEdit');
Route::get('/delete-desa/{id}', 'DesaController@deleteDesa');
Route::post('/add-desa', 'DesaController@addDesa');
Route::post('/edit-desa', 'DesaController@editDesa');
