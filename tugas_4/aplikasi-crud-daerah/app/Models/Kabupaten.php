<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'kabupaten';

    protected $fillable = [
        'nama',
        'provinsi_id',
    ];

    public function provinsi()
    {
        return $this->belongsTo('App\Models\Provinsi');
    }

    public function kecamatan()
    {
        return $this->hasMany('App\Models\Kecamatan', 'kabupaten_id');
    }

    public function setNamaAttribute($value)
    {
        $this->attributes['nama'] = strtolower($value);
    }

    public function getNamaAttribute($value)
    {
        return ucwords($value);
    }
}
