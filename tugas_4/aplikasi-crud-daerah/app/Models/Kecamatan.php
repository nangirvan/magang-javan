<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'kecamatan';

    protected $fillable = [
        'nama',
        'kabupaten_id',
    ];

    public function kabupaten()
    {
        return $this->belongsTo('App\Models\Kabupaten');
    }

    public function desa()
    {
        return $this->hasMany('App\Models\Desa', 'kecamatan_id');
    }

    public function setNamaAttribute($value)
    {
        $this->attributes['nama'] = strtolower($value);
    }

    public function getNamaAttribute($value)
    {
        return ucwords($value);
    }
}
