<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'provinsi';

    protected $fillable = [
        'nama',
    ];

    public function kabupaten()
    {
        return $this->hasMany('App\Models\Kabupaten', 'provinsi_id');
    }

    public function setNamaAttribute($value)
    {
        $this->attributes['nama'] = strtolower($value);
    }

    public function getNamaAttribute($value)
    {
        return ucwords($value);
    }
}
