<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Provinsi;

class ProvinsiController extends Controller
{
    public function getAllProvinsi()
    {
        $data = [
            'all_provinsi' => Provinsi::all(),
        ];

        return view('provinsi', $data);
    }

    public function showFormAdd()
    {
        return view('form-add-provinsi');
    }

    public function showFormEdit(Request $request)
    {
        $data = [
            'selected_provinsi' => Provinsi::where('id', $request->id)->get(),
        ];

        return view('form-edit-provinsi', $data);
    }

    public function addProvinsi(Request $request)
    {
        $rules = [
            'nama' => 'required|max:100|unique:provinsi,nama'
        ];
        $messages = [
            'required' => 'Data tidak boleh kosong',
            'nama.max' => 'Nama provinsi tidak boleh melebihi 100 karakter',
            'nama.unique' => 'Provinsi sudah terdaftar',
        ];
        $prepareForValidation = [
            'nama' => strtolower($request->nama),
        ];

        $validator = Validator::make($prepareForValidation, $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $validated = $validator->validated();
        Provinsi::create($validated);

        return redirect('/provinsi')->with('success', 'Tambah data Provinsi berhasil!');
    }

    public function editProvinsi(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:provinsi,id',
            'nama' => [
                'required',
                'max:100',
                Rule::unique('provinsi')->ignore($request->id),
            ],
        ];
        $messages = [
            'required' => 'Data tidak boleh kosong',
            'integer' => 'Data tidak valid',
            'exists' => 'Data tidak ditemukan',
            'nama.max' => 'Nama provinsi tidak boleh melebihi 100 karakter',
            'nama.unique' => 'Provinsi sudah terdaftar',
        ];
        $prepareForValidation = [
            'id' => $request->id,
            'nama' => strtolower($request->nama),
        ];

        $validator = Validator::make($prepareForValidation, $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $validated = $validator->validated();
        Provinsi::where('id', $validated['id'])->update($validated);

        return redirect('/provinsi')->with('success', 'Edit data Provinsi berhasil!');
    }

    public function deleteProvinsi(Request $request)
    {
        Provinsi::where('id', $request->id)->delete();

        return redirect('/provinsi')->with('success', 'Hapus data Provinsi berhasil!');
    }
}
