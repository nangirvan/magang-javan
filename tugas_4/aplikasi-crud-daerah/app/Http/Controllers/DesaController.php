<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Desa;
use App\Models\Kecamatan;

class DesaController extends Controller
{
    public function getAllDesa()
    {
        $data = [
            'all_desa' => Desa::all(),
        ];

        return view('desa', $data);
    }

    public function showFormAdd()
    {
        $data = [
            'all_kecamatan' => Kecamatan::all(),
        ];

        return view('form-add-desa', $data);
    }

    public function showFormEdit(Request $request)
    {
        $data = [
            'all_kecamatan' => Kecamatan::all(),
            'selected_desa' => Desa::where('id', $request->id)->get(),
        ];

        return view('form-edit-desa', $data);
    }

    public function addDesa(Request $request)
    {
        $prepareForValidation = [
            'kecamatan_id' => $request->kecamatan_id,
            'nama' => strtolower($request->nama),
        ];
        $rules = [
            'kecamatan_id' => 'required|integer|exists:kecamatan,id',
            'nama' => 'required|max:100|unique:desa,nama',
        ];
        $messages = [
            'required' => 'Data tidak boleh kosong',
            'integer' => 'Data tidak valid',
            'exists' => 'Data tidak ditemukan',
            'nama.max' => 'Nama Desa tidak boleh lebih dari 100 karakter',
            'nama.unique' => 'Desa sudah terdaftar',
        ];

        $validator = Validator::make($prepareForValidation, $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $validated = $validator->validated();
        Desa::create($validated);

        return redirect('/desa')->with('success', 'Tambah data Desa berhasil!');
    }

    public function editDesa(Request $request)
    {
        $prepareForValidation = [
            'id' => $request->id,
            'kecamatan_id' => $request->kecamatan_id,
            'nama' => strtolower($request->nama),
        ];
        $rules = [
            'id' => 'required|integer|exists:desa,id',
            'kecamatan_id' => 'required|integer|exists:kecamatan,id',
            'nama' => [
                'required',
                'max:100',
                Rule::unique('desa')->ignore($request->id),
            ],
        ];
        $messages = [
            'required' => 'Data tidak boleh kosong',
            'integer' => 'Data tidak valid',
            'exists' => 'Data tidak ditemukan',
            'nama.max' => 'Nama Desa tidak boleh melebihi 100 karakter',
            'nama.unique' => 'Desa sudah terdaftar',
        ];

        $validator = Validator::make($prepareForValidation, $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $validated = $validator->validated();
        Desa::where('id', $validated['id'])->update($validated);

        return redirect('/desa')->with('success', 'Edit data Desa berhasil!');
    }

    public function deleteDesa(Request $request)
    {
        Desa::where('id', $request->id)->delete();

        return redirect('/desa')->with('success', 'Hapus data Desa berhasil!');
    }
}
