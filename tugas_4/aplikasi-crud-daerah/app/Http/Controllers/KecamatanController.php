<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Kecamatan;
use App\Models\Kabupaten;

class KecamatanController extends Controller
{
    public function getAllKecamatan()
    {
        $data = [
            'all_kecamatan' => Kecamatan::all(),
        ];

        return view('kecamatan', $data);
    }

    public function showFormAdd()
    {
        $data = [
            'all_kabupaten' => Kabupaten::all(),
        ];

        return view('form-add-kecamatan', $data);
    }

    public function showFormEdit(Request $request)
    {
        $data = [
            'all_kabupaten' => Kabupaten::all(),
            'selected_kecamatan' => Kecamatan::where('id', $request->id)->get(),
        ];

        return view('form-edit-kecamatan', $data);
    }

    public function addKecamatan(Request $request)
    {
        $prepareForValidation = [
            'kabupaten_id' => $request->kabupaten_id,
            'nama' => strtolower($request->nama),
        ];
        $rules = [
            'kabupaten_id' => 'required|integer|exists:kabupaten,id',
            'nama' => 'required|max:100|unique:kecamatan,nama',
        ];
        $messages = [
            'required' => 'Data tidak boleh kosong',
            'integer' => 'Data tidak valid',
            'exists' => 'Data tidak ditemukan',
            'nama.max' => 'Nama Kecamatan tidak boleh lebih dari 100 karakter',
            'nama.unique' => 'Kecamatan sudah terdaftar',
        ];

        $validator = Validator::make($prepareForValidation, $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $validated = $validator->validated();
        Kecamatan::create($validated);

        return redirect('/kecamatan')->with('success', 'Tambah data Kecamatan berhasil!');
    }

    public function editKecamatan(Request $request)
    {
        $prepareForValidation = [
            'id' => $request->id,
            'kabupaten_id' => $request->kabupaten_id,
            'nama' => strtolower($request->nama),
        ];
        $rules = [
            'id' => 'required|integer|exists:kecamatan,id',
            'kabupaten_id' => 'required|integer|exists:kabupaten,id',
            'nama' => [
                'required',
                'max:100',
                Rule::unique('kecamatan')->ignore($request->id),
            ],
        ];
        $messages = [
            'required' => 'Data tidak boleh kosong',
            'integer' => 'Data tidak valid',
            'exists' => 'Data tidak ditemukan',
            'nama.max' => 'Nama Kecamatan tidak boleh melebihi 100 karakter',
            'nama.unique' => 'Kecamatan sudah terdaftar',
        ];

        $validator = Validator::make($prepareForValidation, $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $validated = $validator->validated();
        Kecamatan::where('id', $validated['id'])->update($validated);

        return redirect('/kecamatan')->with('success', 'Edit data Kecamatan berhasil!');
    }

    public function deleteKecamatan(Request $request)
    {
        Kecamatan::where('id', $request->id)->delete();

        return redirect('/kecamatan')->with('success', 'Hapus data Kecamatan berhasil!');
    }
}
