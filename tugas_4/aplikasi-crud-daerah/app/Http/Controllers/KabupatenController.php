<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Kabupaten;
use App\Models\Provinsi;

class KabupatenController extends Controller
{
    public function getAllKabupaten()
    {
        $data = [
            'all_kabupaten' => Kabupaten::all(),
        ];

        return view('kabupaten', $data);
    }

    public function showFormAdd()
    {
        $data = [
            'all_provinsi' => Provinsi::all(),
        ];

        return view('form-add-kabupaten', $data);
    }

    public function showFormEdit(Request $request)
    {
        $data = [
            'all_provinsi' => Provinsi::all(),
            'selected_kabupaten' => Kabupaten::where('id', $request->id)->get(),
        ];

        return view('form-edit-kabupaten', $data);
    }

    public function addKabupaten(Request $request)
    {
        $prepareForValidation = [
            'provinsi_id' => $request->provinsi_id,
            'nama' => strtolower($request->nama),
        ];
        $rules = [
            'provinsi_id' => 'required|integer|exists:provinsi,id',
            'nama' => 'required|max:100|unique:kabupaten,nama',
        ];
        $messages = [
            'required' => 'Data tidak boleh kosong',
            'integer' => 'Data tidak valid',
            'exists' => 'Data tidak ditemukan',
            'nama.max' => 'Nama Kabupaten tidak boleh lebih dari 100 karakter',
            'nama.unique' => 'Kabupaten sudah terdaftar',
        ];

        $validator = Validator::make($prepareForValidation, $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $validated = $validator->validated();
        Kabupaten::create($validated);

        return redirect('/kabupaten')->with('success', 'Tambah data Kabupaten berhasil!');
    }

    public function editKabupaten(Request $request)
    {
        $prepareForValidation = [
            'id' => $request->id,
            'provinsi_id' => $request->provinsi_id,
            'nama' => strtolower($request->nama),
        ];
        $rules = [
            'id' => 'required|integer|exists:kabupaten,id',
            'provinsi_id' => 'required|integer|exists:provinsi,id',
            'nama' => [
                'required',
                'max:100',
                Rule::unique('kabupaten')->ignore($request->id),
            ],
        ];
        $messages = [
            'required' => 'Data tidak boleh kosong',
            'integer' => 'Data tidak valid',
            'exists' => 'Data tidak ditemukan',
            'nama.max' => 'Nama kabupaten tidak boleh melebihi 100 karakter',
            'nama.unique' => 'Kabupaten sudah terdaftar',
        ];

        $validator = Validator::make($prepareForValidation, $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $validated = $validator->validated();
        Kabupaten::where('id', $validated['id'])->update($validated);

        return redirect('/kabupaten')->with('success', 'Edit data Kabupaten berhasil!');
    }

    public function deleteKabupaten(Request $request)
    {
        Kabupaten::where('id', $request->id)->delete();

        return redirect('/kabupaten')->with('success', 'Hapus data Kabupaten berhasil!');
    }
}
