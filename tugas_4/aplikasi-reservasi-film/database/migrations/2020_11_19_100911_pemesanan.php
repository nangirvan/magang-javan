<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pemesanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanan', function (Blueprint $table) {
            $table->id();
            $table->foreignId('penonton_id')
                  ->constrained('penonton')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->foreignId('film_id')
                  ->constrained('film')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->dateTimeTz('jam_tayang', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemesanan');
    }
}
