<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FilmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('film')->insert(
            [
                ['judul' => 'interstellar'],
                ['judul' => 'avengers end game'],
                ['judul' => 'fantastic beast and where to find them'],
            ]
        );
    }
}
