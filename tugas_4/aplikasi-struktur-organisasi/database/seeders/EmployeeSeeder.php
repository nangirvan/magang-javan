<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employee')->insert([
            ['nama' => 'pak budi', 'atasan_id' => null, 'company_id' => 1],
            ['nama' => 'pak tono', 'atasan_id' => 1, 'company_id' => 1],
            ['nama' => 'pak totok', 'atasan_id' => 1, 'company_id' => 1],
            ['nama' => 'bu sinta', 'atasan_id' => 2, 'company_id' => 1],
            ['nama' => 'bu novi', 'atasan_id' => 3, 'company_id' => 1],
            ['nama' => 'andre', 'atasan_id' => 4, 'company_id' => 1],
            ['nama' => 'dono', 'atasan_id' => 4, 'company_id' => 1],
            ['nama' => 'ismir', 'atasan_id' => 5, 'company_id' => 1],
            ['nama' => 'anto', 'atasan_id' => 5, 'company_id' => 1],
        ]);
    }
}
