<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

// Company
Route::get('/companies', 'CompanyController@getAllCompany')->name('companies');
Route::get('/add-company', 'CompanyController@showFormAdd');
Route::get('/edit-company/{id}', 'CompanyController@showFormEdit');
Route::get('/delete-company/{id}', 'CompanyController@deleteCompany');

Route::post('/add-company', 'CompanyController@addCompany');
Route::post('/edit-company', 'CompanyController@editCompany');


// Employee
Route::get('/employees', 'EmployeeController@getAllEmployee')->name('employees');
Route::get('/add-employee', 'EmployeeController@showFormAdd');
Route::get('/edit-employee/{id}', 'EmployeeController@showFormEdit');
Route::get('/delete-employee/{id}', 'EmployeeController@deleteEmployee');

Route::post('/add-employee', 'EmployeeController@addEmployee');
Route::post('/edit-employee', 'EmployeeController@editEmployee');


// Export
Route::get('/export-pdf', 'ExportController@exportToPdf');
Route::get('/export-excel', 'ExportController@exportToExcel');
