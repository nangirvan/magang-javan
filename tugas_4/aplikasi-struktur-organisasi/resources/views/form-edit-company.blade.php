@extends('layouts.app')

@section('title', 'Edit Company')

@section('content')
    <h5 class="my-4 text-center">Form Edit</h5>
    <form action="/edit-company" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $selected_company[0]->id }}">
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ $selected_company[0]->nama }}" required>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inAlamat">Alamat</label>
            <input class="form-control @error('alamat') is-invalid @enderror" type="text" name="alamat" id="inAlamat" value="{{ $selected_company[0]->alamat }}" required>
            @error('alamat')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Submit</button>
            <a class="btn btn-danger float-right" href="/companies">Cancel</a>
        </div>
    </form>
@endsection
