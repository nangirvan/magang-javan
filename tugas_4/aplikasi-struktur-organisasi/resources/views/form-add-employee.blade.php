@extends('layouts.app')

@section('title', 'Add Employee')

@section('content')
    <h5 class="my-4 text-center">Form Add Employee</h5>
    <form action="/add-employee" method="post">
        @csrf
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ old('nama') }}" required>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inCompany">Company</label>
            <select class="form-control @error('company_id') is-invalid @enderror" name="company_id" id="inCompany" required>
                <option value="">Select company ...</option>
                @foreach($companies as $company)
                    <option value="{{ $company->id }}">{{ $company->nama }}</option>
                @endforeach
            </select>
            @error('company_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inAtasan">Atasan</label>
            <select class="form-control @error('atasan_id') is-invalid @enderror" name="atasan_id" id="inAtasan" required>
                <option value="">Select atasan ...</option>
                @foreach($employees as $employee)
                    <option value="{{ $employee->id }}">{{ $employee->nama }}</option>
                @endforeach
            </select>
            @error('atasan_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Add Employee</button>
            <a href="/employees" class="btn btn-danger float-right" type="submit">Cancel</a>
        </div>
    </form>
@endsection
