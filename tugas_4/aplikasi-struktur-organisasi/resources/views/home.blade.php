@extends('layouts.app')

@section('title', 'Home')

@section('content')

    <div class="text-center my-4">
        <h2 class="my-4">Aplikasi Struktur Organisasi</h2>
        <hr style="width: 75%">
        <a class="btn btn-lg btn-primary my-2 mx-2" href="/companies">CRUD Company</a>
        <a class="btn btn-lg btn-primary my-2 mx-2" href="/employees">CRUD Employee</a>
    </div>

    <div class="text-center my-4">
        <h5>Export Data</h5>
        <hr style="width: 75%">
        <a class="btn btn-lg btn-primary my-2 mx-2" href="/export-pdf">Export to PDF</a>
        <a class="btn btn-lg btn-primary my-2 mx-2" href="/export-excel">Export to Excel</a>
    </div>
@endsection

