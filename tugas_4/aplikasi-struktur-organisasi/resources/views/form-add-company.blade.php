@extends('layouts.app')

@section('title', 'Add Company')

@section('content')
    <h5 class="my-4 text-center">Form Add Company</h5>
    <form action="/add-company" method="post">
        @csrf
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ old('nama') }}" required>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inAlamat">Alamat</label>
            <input class="form-control @error('alamat') is-invalid @enderror" type="text" name="alamat" id="inAlamat" value="{{ old('alamat') }}" required>
            @error('alamat')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Add Company</button>
            <a href="/companies" class="btn btn-danger float-right" type="submit">Cancel</a>
        </div>
    </form>
@endsection
