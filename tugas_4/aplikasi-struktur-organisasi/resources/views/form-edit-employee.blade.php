@extends('layouts.app')

@section('title', 'Edit Employee')

@section('content')
    <h5 class="my-4 text-center">Form Edit</h5>
    <form action="/edit-employee" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $selected_employee[0]->id }}">
        <div class="form-group">
            <label for="inNama">Nama</label>
            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="inNama" value="{{ $selected_employee[0]->nama }}" required>
            @error('nama')
            <div class="invalid-feedback">
                {{ $message}}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inCompany">Company</label>
            <select class="form-control @error('company_id') is-invalid @enderror" id="inCompany" name="company_id">
                @foreach($companies as $company)
                    @if($company->id == $selected_employee[0]->company_id)
                        <option value="{{ $company->id }}" selected>{{ $company->nama }}</option>
                    @else
                        <option value="{{ $company->id }}">{{ $company->nama }}</option>
                    @endif
                @endforeach
            </select>
            @error('company_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="inAtasan">Atasan</label>
            <select class="form-control @error('atasan_id') is-invalid @enderror" id="inAtasan" name="atasan_id">
                @foreach($employees as $employee)
                    @if($employee->id === $selected_employee[0]->atasan_id)
                        <option value="{{ $employee->id }}" selected>{{ $employee->nama }}</option>
                    @else
                        <option value="{{ $employee->id }}">{{ $employee->nama }}</option>
                    @endif
                @endforeach
            </select>
            @error('atasan_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="my-4">
            <button class="btn btn-primary float-left" type="submit">Submit</button>
            <a class="btn btn-danger float-right" href="/employees">Cancel</a>
        </div>
    </form>
@endsection
