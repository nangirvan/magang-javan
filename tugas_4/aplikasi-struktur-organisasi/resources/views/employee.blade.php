@extends('layouts.app')

@section('title', 'Employee')

@section('content')
    @if(session('success'))
    <div class="alert alert-warning alert-dismissible fade show my-4" role="alert">
        <strong>Success!</strong> {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <div class="my-4">
        <h3 class="float-left">Employee</h3>
        <a href="/add-employee" class="float-right btn btn-primary text-white mb-4 font-weight-bold">Add Data</a>
        <a href="/" class="float-right btn btn-secondary text-white mb-4 mx-2">Home</a>
    </div>

    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th>Id</th>
                <th>Nama</th>
                <th>Atasan</th>
                <th>Company</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($employees as $employee)
            <tr>
                <td>{{ $employee->id }}</td>
                <td>{{ $employee->nama }}</td>
                <td>
                    @if(isset($employee->atasan))
                        {{ $employee->atasan->nama }}
                    @endif
                </td>
                <td>{{ $employee->company->nama }}</td>
                <td>
                    <a href="/edit-employee/{{ $employee->id }}" class="btn btn-warning text-white font-italic">Edit</a>
                    <a href="/delete-employee/{{ $employee->id }}" class="btn btn-danger text-white font-italic">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
