## Aplikasi Struktur Organisasi

Route "/" berisi tombol navigasi ke 2 aplikasi berikut :
- CRUD Employee
- CRUD Company

Selain itu terdapat 2 fitur export data Employee dan Company :
- Export to PDF
- Export to Excel


## CRUD Employee

- Route utama yang digunakan adalah "/employees"
- Controller nya adalah "EmployeeController"
- Tentang database :
    - Model nya adalah "Employee"
    - Migration nya adalah "employee"
    - Seeder nya adalah "EmployeeSeeder"
- Tentang views :
    - View untuk menampilkan semua employee adalah "employee"
    - View untuk tambah employee adalah "form-add-employee"
    - View untuk edit employee adalah "form-edit-employee"
- Tentang validation :
    - Validasi tambah employee menggunakan class "NewEmployeeValidation"
    - Validasi edit employee menggunakan Validator Manual
    
    
## CRUD Company

- Route utama yang digunakan adalah "/companies"
- Controller nya adalah "CompanyController"
- Tentang database :
    - Model nya adalah "Company"
    - Migration nya adalah "company"
    - Seeder nya adalah "CompanySeeder"
- Tentang views :
    - View untuk menampilkan semua company adalah "company"
    - View untuk tambah company adalah "form-add-company"
    - View untuk edit company adalah "form-edit-company"
- Tentang validation :
    - Validasi tambah company menggunakan class "NewCompanyValidation"
    - Validasi edit company menggunakan Validator Manual


## Fitur Export Data Employee dan Company

- Route yang digunakan adalah "/export-pdf" dan "/export-excel"
- Controller yang digunakan adalah "ExportController"
- View yang digunakan adalah "home"
- Class Export nya adalah "EmployeesExport"
- Tentang data :
    - Data di format pada controller
    - Setiap employee dikelompokkan berdasarkan posisinya lalu digabung semuanya ke dalam array
    - Array berisi semua data employee digunakan sebagai parameter class export
- Dependensi :
    - maatwebsite/excel
    - dompdf/dompdf
