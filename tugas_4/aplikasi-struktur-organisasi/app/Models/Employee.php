<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'employee';

    protected $fillable = [
        'nama',
        'atasan_id',
        'company_id',
    ];

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function setNamaAttribute($value)
    {
        return $this->attributes['nama'] = strtolower($value);
    }

    public function getNamaAttribute($value)
    {
        return ucwords($value);
    }

}
