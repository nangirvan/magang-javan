<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'company';

    protected $fillable = [
        'nama',
        'alamat',
    ];

    public function employee()
    {
        return $this->hasMany('App\Models\Employee');
    }

    public function setNamaAttribute($value)
    {
        $this->attributes['nama'] = strtolower($value);
    }

    public function setAlamatAttribute($value)
    {
        $this->attributes['alamat'] = strtolower($value);
    }

    public function getNamaAttribute($value)
    {
        return str_replace('Pt', 'PT', ucwords($value));
    }

    public function getAlamatAttribute($value)
    {
        return ucwords($value);
    }
}
