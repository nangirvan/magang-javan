<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewCompanyValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|unique:company,nama',
            'alamat' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Data tidak boleh kosong',
            'nama.unique' => 'Company sudah terdaftar',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
           'nama' => strtolower($this->nama),
        ]);
    }
}
