<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewEmployeeValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|unique:employee,nama',
            'atasan_id' => 'required|integer|exists:employee,id',
            'company_id' => 'required|integer|exists:company,id',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Data tidak boleh kosong',
            'nama.unique' => 'Employee sudah terdaftar',
            'integer' => ':attribute tidak valid',
            'exists' => ':attribute tidak terdaftar',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
           'nama' => strtolower($this->nama),
        ]);
    }
}
