<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NewCompanyValidation;
use App\Models\Company;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    public function getAllCompany()
    {
        $companies = Company::all()->sortBy('id');

        $data = [
            'companies' => $companies,
        ];

        return view('company', $data);
    }

    public function showFormAdd()
    {
        return view('form-add-company');
    }

    public function addCompany(NewCompanyValidation $request)
    {
        $validated = $request->validated();

        Company::create($validated);

        return redirect('companies')->with('success', 'Tambah data Company berhasil!');
    }

    public function showFormEdit(Request $request)
    {
        $company = Company::where('id', $request->id)->get();
        $data = [
            'selected_company' => $company,
        ];
        return view('form-edit-company', $data);
    }

    public function editCompany(Request $request)
    {
        /*
         * Validation
         *
         * dibuat manual karena membutuhkan rule unique dengan mengabaikan employee yang akan di edit,
         * karena saat dicoba dibuatkan class validation sendiri tidak bisa
         *
         * */
        $rules = [
            'id' => 'required|integer|exists:company,id',
            'nama' => 'required|unique:company,nama,'.$request->id,
            'alamat' => 'required',
        ];
        $messages = [
            'required' => 'Input tidak boleh kosong',
            'integer' => ':attribute tidak valid',
            'exists' => ':attribute tidak terdaftar',
            'nama.unique' => 'Company sudah terdaftar',
        ];
        $prepareForValidation = [
            'id' => $request->id,
            'nama' => strtolower($request->nama),
            'alamat' => strtolower($request->alamat),
        ];

        $validator = Validator::make($prepareForValidation, $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        // Update data company
        $validated = $validator->validated();
        $data = [
            'nama' => $validated['nama'],
            'alamat' => $validated['alamat'],
        ];

        Company::where('id', $validated['id'])->update($data);

        return redirect('companies')->with('success', 'Edit data Company berhasil!');
    }

    public function deleteCompany(Request $request)
    {
        Company::where('id', $request->id)->delete();

        return redirect('companies')->with('success', 'Hapus data Company berhasil!');
    }

}
