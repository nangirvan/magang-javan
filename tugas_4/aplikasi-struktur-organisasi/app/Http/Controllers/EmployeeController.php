<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests\NewEmployeeValidation;
use App\Models\Employee;
use App\Models\Company;


class EmployeeController extends Controller
{
    public function getAllEmployee()
    {
        $employees = Employee::all()->sortBy('id');

        foreach ($employees as $employee) {
            $atasan = Employee::where('id', $employee['atasan_id'])->get();
            $company = Employee::find($employee['id'])->company()->get();

            $employee['atasan'] = isset($atasan[0]) ? $atasan[0] : null;
            $employee['company'] = isset($company[0]) ? $company[0] : null;
        }

        $data = [
            'employees' => $employees,
        ];

        return view('employee', $data);
    }

    public function showFormAdd()
    {
        $companies = Company::all()->sortBy('id');
        $employees = Employee::all()->sortBy('id');

        $data = [
            'companies' => $companies,
            'employees' => $employees,
        ];

        return view('form-add-employee', $data);
    }

    public function addEmployee(NewEmployeeValidation $request)
    {
        $validated = $request->validated();

        Employee::create($validated);

        return redirect('employees')->with('success', 'Tambah data Employee berhasil!');
    }

    public function showFormEdit(Request $request)
    {
        $employee = Employee::where('id', $request->id)->get();
        $companies = Company::all()->sortBy('id');
        $employees = Employee::where('id', '<>', $request->id)->orderBy('id')->get();

        $data = [
            'companies' => $companies,
            'employees' => $employees,
            'selected_employee' => $employee,
        ];

        return view('form-edit-employee', $data);
    }

    public function editEmployee(Request $request)
    {
        // Validation
        /*
         * dibuat manual karena membutuhkan rule unique dengan mengabaikan employee yang akan di edit,
         * karena saat dicoba dibuatkan class validation sendiri tidak bisa
         * */
        $rules = [
            'id' => 'required|integer|exists:employee,id',
            'nama' => 'required|unique:employee,nama,'.$request->id,
            'atasan_id' => 'required|integer|exists:employee,id',
            'company_id' => 'required|integer|exists:company,id',
        ];
        $messages = [
            'required' => 'Input tidak boleh kosong',
            'exists' => ':attribute tidak terdaftar',
            'integer' => ':atribute tidak valid',
            'nama.unique' => 'Employee sudah terdaftar',
        ];
        $prepareForValidation = [
            'id' => $request->id,
            'nama' => strtolower($request->nama),
            'atasan_id' => $request->atasan_id,
            'company_id' => $request->company_id,
        ];

        $validator = Validator::make($prepareForValidation, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        // Update data employee
        $validated = $validator->validated();
        $data = [
            'nama' => $validated['nama'],
            'atasan_id' => $validated['atasan_id'],
            'company_id' => $validated['company_id'],
        ];

        Employee::where('id', $validated['id'])->update($data);

        return redirect('employees')->with('success', 'Edit data Employee berhasil!');
    }

    public function deleteEmployee(Request $request)
    {
        Employee::where('id', '=', $request->id)->delete();

        return redirect('employees')->with('success', 'Hapus data Employee berhasil!');
    }

}
