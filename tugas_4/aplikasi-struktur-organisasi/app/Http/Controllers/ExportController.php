<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Exports\EmployeesExport;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    public function getCEO()
    {
        $data;

        foreach (Employee::where('atasan_id', null)->get() as $ceo) {
            $data[] = [
                'id' => $ceo['id'],
                'nama' => $ceo['nama'],
                'posisi' => 'CEO',
                'perusahaan' => Employee::find($ceo['id'])->company()->get()[0]['nama'],
            ];
        }

        return $data;
    }

    public function getDirektur()
    {
        $data;

        foreach ($this->getCEO() as $ceo) {
            foreach (Employee::where('atasan_id', $ceo['id'])->get() as $direktur) {
                $data[] = [
                    'id' => $direktur['id'],
                    'nama' => $direktur['nama'],
                    'posisi' => 'Direktur',
                    'perusahaan' => Employee::find($direktur['id'])->company()->get()[0]['nama'],
                ];
            }
        }

        return $data;
    }

    public function getManager()
    {
        $data;

        foreach ($this->getDirektur() as $direktur) {
            foreach (Employee::where('atasan_id', $direktur['id'])->get() as $manager) {
                $data[] = [
                    'id' => $manager['id'],
                    'nama' => $manager['nama'],
                    'posisi' => 'Manager',
                    'perusahaan' => Employee::find($manager['id'])->company()->get()[0]['nama'],
                ];
            }
        }

        return $data;
    }

    public function getStaff()
    {
        $data;

        foreach ($this->getManager() as $manager) {
            foreach (Employee::where('atasan_id', $manager['id'])->get() as $staff) {
                $data[] = [
                    'id' => $staff['id'],
                    'nama' => $staff['nama'],
                    'posisi' => 'Staff',
                    'perusahaan' => Employee::find($staff['id'])->company()->get()[0]['nama'],
                ];
            }
        }

        return $data;
    }

    public function exportToPdf()
    {
        $employees = array_merge($this->getCEO(), $this->getDirektur(), $this->getManager(), $this->getStaff());

        return Excel::download(new EmployeesExport($employees), 'employees.pdf');
    }

    public function exportToExcel()
    {
        $employees = array_merge($this->getCEO(), $this->getDirektur(), $this->getManager(), $this->getStaff());

        return Excel::download(new EmployeesExport($employees), 'employees.xlsx');
    }

}
