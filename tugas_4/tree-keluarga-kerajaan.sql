CREATE TABLE kerajaan (
    id varchar(100) DEFAULT NULL,
    nama varchar(100) DEFAULT NULL,
    kelamin varchar(100) DEFAULT NULL,
    parent_id varchar(100) DEFAULT NULL
);

INSERT INTO kerajaan (id,nama,kelamin,parent_id) VALUES
    ('E001','Elizabeth II','female',NULL),
    ('E002','Charles','male','E001'),
    ('E003','Anne','female','E001'),
    ('E004','Andrew','male','E001'),
    ('E005','Edward','male','E001'),
    ('E006','William','male','E002'),
    ('E007','Harry','male','E002'),
    ('E008','Zara','female','E003'),
    ('E009','Peter','male','E003'),
    ('E010','Eugenie','female','E004');

INSERT INTO kerajaan (id,nama,kelamin,parent_id) VALUES
    ('E011','Beatrice','female','E004'),
    ('E012','James','male','E005'),
    ('E013','Louise','female','E005'),
    ('E014','George','male','E006'),
    ('E015','Charlotte','male','E006'),
    ('E016','Louis','male','E006'),
    ('E017','Lena','female','E008'),
    ('E018','Mia','female','E008'),
    ('E019','Isla','female','E009'),
    ('E020','Savannah','female','E009');

INSERT INTO kerajaan (id,nama,kelamin,parent_id) VALUES
    ('M001','Mpu Sindok','male',NULL),
    ('M002','Sri Isanatunggawijaya','male','M001'),
    ('M003','Sri Makutawangsawardhana','male','M002'),
    ('M004','Sri Dharmawangsa','male','M003'),
    ('M005','Gunapriadharmptani','female','M003'),
    ('M006','Airlangga','male','M005'),
    ('M007','Marakata','male','M005'),
    ('M008','Anak Wungsu','male','M005');



-- 1. Generasi awal
SELECT nama FROM kerajaan WHERE parent_id IS NULL;


-- 2. Anak dari seseorang
SELECT nama FROM kerajaan WHERE parent_id = (
    SELECT id FROM kerajaan WHERE nama='Mpu Sindok');

SELECT child.nama
FROM kerajaan seseorang
         JOIN kerajaan child ON child.parent_id = seseorang.id
WHERE seseorang.nama = 'Mpu Sindok';


-- 3. Kakek / Nenek dari seseorang
SELECT nama FROM kerajaan WHERE id = (
    SELECT parent_id FROM kerajaan WHERE id = (
        SELECT parent_id FROM kerajaan WHERE nama='William'));

SELECT grandparent.nama
FROM kerajaan seseorang
    JOIN kerajaan parent ON seseorang.parent_id = parent.id
    JOIN kerajaan grandparent ON parent.parent_id = grandparent.id
WHERE seseorang.nama = 'William';


-- 4. Cucu dari seseorang
SELECT grandchild.nama
FROM kerajaan seseorang
    JOIN kerajaan child ON child.parent_id = seseorang.id
    JOIN kerajaan grandchild ON grandchild.parent_id = child.id
WHERE seseorang.nama = 'Charles';


-- 5. Jumlah anak seseorang
SELECT COUNT(child.id) as jumlah_anak
FROM kerajaan seseorang
    JOIN kerajaan child ON child.parent_id = seseorang.id
WHERE seseorang.nama = 'Elizabeth II';

SELECT parent.nama, COUNT(child.id) as jumlah_anak
FROM kerajaan child
    JOIN kerajaan parent ON child.parent_id = parent.id
GROUP BY parent.nama, child.parent_id;


-- 6. Total keturunan seseorang
WITH RECURSIVE keturunan AS (
    SELECT * FROM kerajaan WHERE nama = 'Charles'
    UNION
    SELECT child.* FROM kerajaan child JOIN keturunan seseorang ON child.parent_id = seseorang.id
) SELECT COUNT(keturunan.id) FROM keturunan WHERE nama <> 'Charles';


-- 7. Total keturunan seseorang berdasarkan kelamin
-- Saat submit terjadi error 403, jadi sintaks UNION diganti U
WITH RECURSIVE keturunan AS (
    SELECT seseorang.* FROM kerajaan seseorang WHERE nama = 'Charles'
    UNION
    SELECT child.* FROM kerajaan child JOIN keturunan parent ON child.parent_id = parent.id
) SELECT kelamin, COUNT(id) as jumlah_keturunan FROM keturunan WHERE nama <> 'Charles' GROUP BY kelamin;