-- Jumlah komplain setiap bulan
SELECT date_part('month', date_received) as bulan, count(complaint_id) as jumlah_komplain
FROM "ConsumerComplaints"
GROUP BY date_part('month', date_received)
ORDER BY date_part('month', date_received);


-- Komplain dengan tags 'Older American'
SELECT *
FROM "ConsumerComplaints"
WHERE tags = 'Older American'
ORDER BY complaint_id;


-- View data company response
CREATE VIEW data_company_response AS
SELECT company,
       SUM(CASE WHEN company_response_to_consumer = 'Closed' THEN 1 ELSE 0 END) closed,
       SUM(CASE WHEN company_response_to_consumer = 'Closed with explanation' THEN 1 ELSE 0 END) closed_with_explanation,
       SUM(CASE WHEN company_response_to_consumer = 'Closed with non-monetary relief' THEN 1 ELSE 0 END) closed_with_non_monetary_relief
FROM "ConsumerComplaints"
GROUP BY company
ORDER BY company;

-- Hasil view data company response
SELECT * FROM data_company_response;