## Tugas 2

1. Hello World
2. Kalkulator Sederhana
3. Ganjil Genap
4. Hitung Vokal

## 1. Hello World
- Route yang digunakan adalah "/hello-world"
- Controller yang digunakan adalah "HelloWorldController"
- View yang digunakan adalah "hello-world"

## 2. Kalkulator Sederhana
- Route yang digunakan adalah "/kalkulator-sederhana", 
    - Route get digunakan untuk menampilkan view 
    - Route post digunakan untuk melakukan perhitungan
- Controller yang digunakan adalah "KalkulatorSederhanaController"
- View yang digunakan adalah "kalkulator-sederhana"
- Terdapat model tersendiri untuk melakukan perhitungan, yaitu model "Calculator"
- Terdapat test class untuk menguji model Calculator, yaitu "CalculatorTest"

## 3. Ganjil Genap
- Route yang digunakan adalah "/ganjil-genap"
    - Route get digunakan untuk menampilkan view
    - Route post digunakan untuk generate bilangan beserta statusnya
- Controller yang digunakan adalah "GanjilGenapController"
- View yang digunakan adalah "ganjil-genap"
- Terdapat model tersendiri untuk melakukan generate, yaitu model "Number"
- Terdapat test class untuk menguji model Number, yaitu "NumberTest"

## 4. Hitung Vokal
- Route yang digunakan adalah "/hitung-vokal"
    - Route get digunakan untuk menampilkan view
    - Route post digunakan untuk memproses perhitungan huruf vokal
- Controller yang digunakan adalah "HitungVokalController"
- View yang digunakan adalah "hitung-vokal"
- Terdapat validation class untuk menghindari xss, yaitu class "HitungVokal"
- Terdapat model tersendiri untuk memproses perhitungan huruf vokal, yaitu model "Letter"
- Terdapat test class untuk menguji model Letter, yaitu "LetterTest"



