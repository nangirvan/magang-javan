<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Number;

class NumberTest extends TestCase
{
    /** @test */
    public function is_even_or_odd_return_the_right_value()
    {
        $number = new Number();
        $expected_ganjil = $number->isEvenOrOdd(1);
        $expected_genap = $number->isEvenOrOdd(2);
        $expected_false = $number->isEvenOrOdd('iv');

        $this->assertEquals('ganjil', $expected_ganjil);
        $this->assertEquals('genap', $expected_genap);
        $this->assertEquals(false, $expected_false);
    }

    /** @test */
    public function generate_with_status_return_the_right_array()
    {
        $number = new Number();

        $result = $number->generateWithStatus(1, 2);
        $expected_size = 2;
        $expected_value = [
            [
                'number' => 1,
                'status' => 'ganjil',
            ],
            [
                'number' => 2,
                'status' => 'genap',
            ],
        ];

        $expected_false = $number->generateWithStatus('i', 3);

        $this->assertCount($expected_size, $result);
        $this->assertEquals($expected_value, $result);
        $this->assertEquals(false, $expected_false);
    }
}
