<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Letter;

class LetterTest extends TestCase
{
    /** @test */
    public function get_vocal_letters_return_the_right_array()
    {
        $letter = new Letter();
        $result = $letter->getVocalLetters('danang irvanulloh');

        $expected_size = 4;
        $expected_value = ['a', 'i', 'u', 'o'];

        $this->assertIsArray($result);
        $this->assertCount($expected_size, $result);
        $this->assertEquals($expected_value, $result);
    }
}
