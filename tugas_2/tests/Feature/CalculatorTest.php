<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Calculator;

class CalculatorTest extends TestCase
{
    /** @test */
    public function get_valid_operators_return_the_right_array()
    {
        $calculator = new Calculator();
        $operators = $calculator->getValidOperators();

        $expected_size = 4;
        $expected_value = ['+', '-', '*', '/'];

        $this->assertIsArray($operators);
        $this->assertCount($expected_size, $operators);
        $this->assertEquals($expected_value, $operators);
    }

    /** @test */
    public function is_operand_a_number_return_the_right_value()
    {
        $calculator = new Calculator();
        $expected_true = $calculator->isOperandANumber(10);
        $expected_false = $calculator->isOperandANumber('x');

        $this->assertEquals(true, $expected_true);
        $this->assertEquals(false, $expected_false);
    }

    /** @test */
    public function is_operator_valid_return_the_right_value()
    {
        $calculator = new Calculator();
        $expected_true = $calculator->isOperatorValid('*');
        $expected_false = $calculator->isOperatorValid('#');

        $this->assertEquals(true, $expected_true);
        $this->assertEquals(false, $expected_false);
    }

    /** @test */
    public function calculate_return_the_right_value()
    {
        $calculator = new Calculator();
        $expected_4 = $calculator->calculate('+', 2, 2);
        $expected_0 = $calculator->calculate('-', 2, 2);
        $expected_6 = $calculator->calculate('*', 2, 3);
        $expected_1 = $calculator->calculate('/', 2, 2);
        $expected_false = $calculator->calculate('/', 2, 0);

        $this->assertEquals(4, $expected_4);
        $this->assertEquals(0, $expected_0);
        $this->assertEquals(6, $expected_6);
        $this->assertEquals(1, $expected_1);
        $this->assertEquals(false, $expected_false);
    }

}
