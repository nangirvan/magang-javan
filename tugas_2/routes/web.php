<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello-world', 'HelloWorldController@index');

Route::get('/kalkulator-sederhana', 'KalkulatorSederhanaController@showForm');
Route::post('/kalkulator-sederhana', 'KalkulatorSederhanaController@calculate');

Route::get('/ganjil-genap', 'GanjilGenapController@showForm');
Route::post('/ganjil-genap', 'GanjilGenapController@generate');

Route::get('/hitung-vokal', 'HitungVokalController@showForm');
Route::post('/hitung-vokal', 'HitungVokalController@count');
