<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Letter;
use App\Http\Requests\HitungVokal;

class HitungVokalController extends Controller
{
    public function showForm()
    {
        return view('hitung-vokal');
    }

    public function count(HitungVokal $request)
    {
        $validated = $request->validated();

        $letter = new Letter();
        $data = [
            'text' => $validated['text'],
            'results' => $letter->getVocalLetters($validated['text']),
        ];

        return view('hitung-vokal', $data);
    }
}
