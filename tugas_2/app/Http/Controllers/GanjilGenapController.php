<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Number;

class GanjilGenapController extends Controller
{
    public function showForm()
    {
        return view('ganjil-genap');
    }

    public function generate(Request $request)
    {
        $a = $request->input('a');
        $b = $request->input('b');

        if ($a < $b) {
            $number = new Number();
            $data = [
                'message' => "Generate number with status from $a to $b",
                'result' => $number->generateWithStatus($a, $b),
            ];
            return view('ganjil-genap', $data);
        }
    }
}
