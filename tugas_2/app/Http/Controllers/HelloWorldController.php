<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelloWorldController extends Controller
{
    public function index()
    {
        $data = [
            'message' => 'Hello World'
        ];
        return view('hello-world', $data);
    }
}
