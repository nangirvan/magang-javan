<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Calculator;

class KalkulatorSederhanaController extends Controller
{
    public function showForm()
    {
        $calculator = new Calculator();

        $data = [
            'operators' => $calculator->getValidOperators(),
        ];

        return view('kalkulator-sederhana', $data);
    }

    public function calculate(Request $request)
    {

        $operator = $request->input('operator');
        $operand1 = $request->input('operand1');
        $operand2 = $request->input('operand2');

        $calculator = new Calculator();
        $result = $calculator->calculate($operator, $operand1, $operand2);

        $data = [
            'operators' => $calculator->getValidOperators(),
            'operation' => "$operand1 $operator $operand2",
            'result' => ($result) ? $result : 'Tidak bisa dilakukan'
        ];

        return view('kalkulator-sederhana', $data);
    }
}
