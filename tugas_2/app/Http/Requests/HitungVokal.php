<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HitungVokal extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'text' => 'required|alpha_num',
        ];
    }

    public function messages()
    {
        return [
            'text.required' => 'Text is required',
            'text.alpha_num' => 'Oops! Something is wrong!',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'text' => str_replace(' ', '', $this->text),
        ]);
    }
}
