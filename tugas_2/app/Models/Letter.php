<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Letter
{
    public function getVocalLetters($str)
    {
        $vocal_letter = ['a', 'i', 'u', 'e', 'o'];
        $result = [];

        foreach ($vocal_letter as $letter) {
            if (strpos($str, $letter) !== false) {
                $result[] = $letter;
            }
        }

        return $result;
    }
}
