<?php

namespace App\Models;

class Calculator
{
    private $valid_operators = ['+', '-', '*', '/'];

    public function getValidOperators()
    {
        return $this->valid_operators;
    }

    public function isOperandANumber($operand)
    {
        return (is_numeric($operand)) ? true : false;
    }

    public function isOperatorValid($operator)
    {
        return (in_array($operator, $this->valid_operators)) ? true : false;
    }

    public function calculate($operator, $operand1, $operand2)
    {
        $result;

        if ($this->isOperatorValid($operator) && $this->isOperandANumber($operand1) && $this->isOperandANumber($operand2)) {
            switch ($operator) {
                case '+' :
                    $result = $operand1 + $operand2;
                    break;
                case '-' :
                    $result = $operand1 - $operand2;
                    break;
                case '*' :
                    $result = $operand1 * $operand2;
                    break;
                case '/' :
                    $result = ($operand2 != 0) ? ($operand1 / $operand2) : false;
                    break;
            }
        }

        return $result;
    }
}
