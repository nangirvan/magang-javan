<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Number
{
    public function isEvenOrOdd($number)
    {
        if (is_numeric($number)) {
            return ($number % 2 == 0) ? 'genap' : 'ganjil';
        }
        return false;
    }

    public function generateWithStatus($a, $b)
    {
        if (is_numeric($a) && is_numeric($b)) {
            $result = [];
            for ($i=$a; $i<=$b; $i++) {
                $generated_value = [
                    'number' => $i,
                    'status' => $this->isEvenOrOdd($i),
                ];
                $result[] = $generated_value;
            }
            return $result;
        }
        return false;
    }
}
