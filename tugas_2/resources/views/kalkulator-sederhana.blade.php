<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kalkulator Sederhana | Tugas 2</title>
</head>
<body>
    <h4>
        Operation :
        @if (isset($operation))
            {{ $operation }}
        @endif
    </h4>
    <h4>
        Result :
        @if (isset($result))
            {{ $result }}
        @endif
    </h4>

    <form action="/kalkulator-sederhana" method="post">
        @csrf
        <input type="number" name="operand1" required>
        <select name="operator" required>
            <option value="" selected>Operator</option>
            @foreach($operators as $operator)
                <option value="{{ $operator }}">{{ $operator }}</option>
            @endforeach
        </select>
        <input type="number" name="operand2" required>
        <br>
        <br>
        <button type="submit">Calculate</button>
    </form>
</body>
</html>
