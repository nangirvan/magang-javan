<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hitung Vokal | Tugas 2</title>
</head>
<body>

    @error('text')
        <h3>{{ $message }}</h3>
    @enderror

    <h5>
        Text :
        @if(isset($text))
            {{ $text }}
        @endif
    </h5>

    <h5>
        Vocal Letter :
        @if(isset($results))
            {{ count($results) }}
            =>
            @foreach($results as $result)
                {{ $result }}
            @endforeach
        @endif
    </h5>

    <form action="/hitung-vokal" method="post">
        @csrf
        <textarea name="text">{{ old('text') }}</textarea>
        <br>
        <button type="submit">Count</button>
    </form>

</body>
</html>
