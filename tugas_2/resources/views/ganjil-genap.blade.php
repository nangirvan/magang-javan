<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ganjil Genap | Tugas 2</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        #result {
            margin-top: 25px;
        }

        .column-title {
            padding: 10px;
        }

        .column-content {
            padding: 10px;
        }
    </style>
</head>
<body>

    <form action="/ganjil-genap" method="post">
        @csrf
        <input type="number" name="a" required>
        <input type="number" name="b" required>
        <button type="submit">Generate</button>
    </form>

    @if(isset($message))
        <h4>{{ $message }}</h4>
    @endif
    <table id="result">
        <tr>
            <th class="column-title">Number</th>
            <th class="column-title">Status</th>
        </tr>
        @if(isset($result))
            @foreach($result as $number)
                <tr>
                    <td class="column-content">{{ $number['number'] }}</td>
                    <td class="column-content">{{ ucfirst($number['status']) }}</td>
                </tr>
            @endforeach
        @endif
    </table>

</body>
</html>
